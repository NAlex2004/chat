/*
 * How to determine the current server time
 * using only xlib.
 *
 * Issues of general relativity may apply. :)
 *
 * gcc -lX11 pingwindow.c -o pingwindow
 */
#include <X11/Xlib.h>
#include <X11/Xatom.h>

unsigned long xservertime()
{
  Display *display;
  Window pingingWindow;
  XEvent propertyEvent;
  XSetWindowAttributes attrs;
  unsigned long res;

  display = XOpenDisplay (NULL);

  attrs.override_redirect = True;
  attrs.event_mask = PropertyChangeMask;

  pingingWindow = XCreateWindow (display,
		 XRootWindow (display, 0), /* parent */
		 -100, -100, 1, 1, /* off-screen */
		 0,
		 CopyFromParent,
		 CopyFromParent,
		 (Visual *)CopyFromParent,
		 CWOverrideRedirect | CWEventMask,
		 &attrs);

  /* Change a property.  XA_PRIMARY is never really
   * used for properties, so it's safe.
   */
  XChangeProperty (display,
	   pingingWindow,
	   XA_PRIMARY, XA_STRING, 8,
	   PropModeAppend, NULL, 0);

  /* Pick up the event. */
  XWindowEvent (display,
	pingingWindow,
	PropertyChangeMask,
	&propertyEvent);

   res = ((XPropertyEvent*)&propertyEvent)->time;

  /* If you want to do this often,
   * just keep the window around and
   * don't destroy it.
   */
  XDestroyWindow (display, pingingWindow);
  XCloseDisplay(display);

  return res;
}