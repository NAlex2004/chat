unit xwin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, x, xatom, xlib, types{, process};

{$L libxtime-static.a}

function xservertime():LongInt; cdecl; external;
function GetActiveWindow(dsp:PDisplay; var awin:PWindow):TStatus;
procedure ActivateWindow(dsp: PDisplay; win: DWORD);
function GetWindowsList(dsp:PDisplay; var wlist:PWindow):DWORD;
function GetWindowDesktopNumber(dsp: PDisplay; win: LongWord): Integer;
function GetCurrentDesktop(dsp:PDisplay; var desk:Integer):TStatus;
function GetWindowName(dsp: PDisplay; win: LongWord): String;

implementation
const
  CompositeRedirectAutomatic=0;
var format_ret: integer;
    nitems_ret: DWord;
    bytes_after_ret: DWord;
    atom, typeret, utf8Atom:TAtom;
    status:TStatus;

    // return name of window win
function GetWindowName(dsp: PDisplay; win: LongWord): String;
var NameUTF8:PChar;
        s:string; i:integer;
begin
      //заголовок окна в UTF8
      Result:='';
      atom:=XInternAtom(dsp, '_NET_WM_NAME', false);
      utf8Atom:= XInternAtom(dsp,'UTF8_STRING',false);
      status:=XGetWindowProperty(dsp, win, atom, 0, 65536, false, utf8Atom,
             @typeret, @format_ret, @nitems_ret, @bytes_after_ret, @NameUTF8);
      s:='';
      for i:=0 to nitems_ret-1 do
      begin
       s:=s+(NameUTF8+i)^;
      end;
      Result:=s;
      XFree(NameUTF8);
end;

function GetCurrentDesktop(dsp:PDisplay; var desk:Integer):TStatus;
var pdesk:PInteger;
begin
      atom:=XInternAtom(dsp, '_NET_CURRENT_DESKTOP', false);
      Result:=XGetWindowProperty(dsp, XDefaultRootWindow(dsp), atom, 0, 1024,
        false, XA_CARDINAL, @typeret, @format_ret, @nitems_ret,
         @bytes_after_ret, @pdesk);
      desk:=pdesk^;
      XFree(pdesk);
end;


function GetWindowDesktopNumber(dsp: PDisplay; win: LongWord): Integer;
var windesk:PInteger;
begin
      // на каком столе окно
      atom:=XInternAtom(dsp, '_NET_WM_DESKTOP', false);
      status:=XGetWindowProperty(dsp, win, atom, 0, 1024,
       false, XA_CARDINAL, @typeret, @format_ret, @nitems_ret,
        @bytes_after_ret, @windesk);
      Result:=windesk^;
      XFree(windesk);
end;
// return awin = active window
function GetActiveWindow(dsp:PDisplay; var awin:PWindow):TStatus;
begin
  atom:=XInternAtom(dsp, '_NET_ACTIVE_WINDOW', false);
  Result:=XGetWindowProperty(dsp, XDefaultRootWindow(dsp), atom, 0, 1024,
   false, XA_WINDOW, @typeret, @format_ret, @nitems_ret,
    @bytes_after_ret, @awin);
end;

// return wlist = list of windows (pointer)
// return value DWORD - windows count
function GetWindowsList(dsp:PDisplay; var wlist:PWindow):DWORD;
begin
  atom:=XInternAtom(dsp, '_NET_CLIENT_LIST', false);
  status:=XGetWindowProperty(dsp, XDefaultRootWindow(dsp), atom, 0, 1024,
   false, XA_WINDOW, @typeret, @format_ret, @nitems_ret,
    @bytes_after_ret, @wlist);
  Result:=nitems_ret;
end;

procedure ActivateWindow(dsp: PDisplay; win: DWORD);
var event:PXEvent;
    attr:TXWindowAttributes;
    //proc: TProcess;
    timestamp: DWORD;
    //sl: TStringList;
begin

{  proc:=TProcess.Create(nil);
  proc.CommandLine:='./xservertime';
  proc.Options:=[poWaitOnExit, poUsePipes];
  proc.Execute;
  sl:=TStringList.Create;
  sl.LoadFromStream(proc.Output);
  timestamp:=StrToInt(Trim(sl.Text));
  sl.Free;
  proc.Free;}
  timestamp:=xservertime;

  New(event);
  XGetWindowAttributes(dsp, win, @attr);
  event^.xclient._type:=ClientMessage;
  event^.xclient.message_type:=XInternAtom(dsp, '_NET_ACTIVE_WINDOW', false);
  event^.xclient.window:=win;
  event^.xclient.format:=32;
  event^.xclient.data.l[0]:=2;
//  event^.xclient.data.l[1]:=QX11Info_appUserTime()+1;
  event^.xclient.data.l[1]:=timestamp;//event^.xproperty.time;
  //gtk_get_current_event_time;//QX11Info_appTime();
  event^.xclient.data.l[2]:=win;
  XSendEvent(dsp, attr.root,
		false, SubstructureNotifyMask or SubstructureRedirectMask,
                event );
  Dispose(event);
end;


end.

