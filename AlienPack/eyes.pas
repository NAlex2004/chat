unit Eyes;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Controls, Graphics, ExtCtrls,
  math, fillunit;

type

  { TNACustomEyes }

  TNACustomEyes = class(TGraphicControl)
  protected
    FAppleColor: TColor;
    FBorder: Boolean;
    FEavesColor: TColor;
    FEyesColor: Tcolor;
    FFGColor:TColor;
    FBGColor:TColor;
    FViewEnabled: Boolean;
    procedure SetAppleColor(AValue: TColor);
    procedure SetEavesColor(AValue: TColor);
    procedure SetEyesColor(AValue: Tcolor);
    procedure SetViewEnabled(const AValue: Boolean);
  protected
    { Protected declarations }
    Bmp:TBitmap;
    Timer:TTimer;
    Dx, Lx, Ly, Rx, Ry:integer;
    OldMx, OldMy, Mx, My:integer;
    Height5, Height2: integer; //height/5 height/2
    F:smallint; // типа расстояние по Z, константа
    FirstTimer:byte;// если опять не будет работать нормально с boolean
// (FIsLoaded сбрасывается на false само), то или FIsLoaded сделать byte,
// или это везде раскоментить
    procedure SetFGColor(AValue: TColor);
    procedure SetBGColor(AValue: TColor);
    procedure Loaded; override;
    //procedure Paint;  override; // Пусть рисуют потомки сами
    function EyesOnTimer:boolean;virtual;// тут мы вычислим для круглого глаза зрачок
    procedure OnTimer(Sender:TObject);virtual;
    property EyesColor: TColor read FEyesColor write SetEyesColor default $00E1E9EA;
    property AppleColor: TColor read FAppleColor write SetAppleColor default clSkyBlue;
    property EavesColor: TColor read FEavesColor write SetEavesColor default clSilver;
    property FGColor: TColor read FFGColor write SetFGColor default clBlack;
    property BGColor: TColor read FBGColor write SetBGColor default clBtnFace;
    property Border: Boolean read FBorder write FBorder default false;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Repaint; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
  published
    { Published declarations }
    property OnClick;//: TNotifyEvent read FOnClick write SetOnClick;
    property OnMouseDown;//: TMouseEvent read FOnMouseDown write SetOnMouseDown;
    property OnDblClick;//: TNotifyEvent read FOnDblClick write SetOnDblClick;
    property OnMouseUp;//: TMouseEvent read FOnMouseUp write SetOnMouseUp;
    property Visible;
    property ViewEnabled: Boolean read FViewEnabled write SetViewEnabled default true;
  end;

  { TNAEyes }
  TNAEyes=class(TNACustomEyes)
     public
       procedure SetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
       constructor Create(AOwner: TComponent); override;
       destructor Destroy; override;
     protected
       procedure Paint;  override;
     published
       property EyesColor;
       property AppleColor;
       property EavesColor;
       property FGColor;
       property BGColor;
       property Border;
  end;



  procedure Register;

implementation




procedure Register;
begin
  {$I eyes_icon.lrs}
  RegisterComponents('AlienPack',[TNAEyes]);
end;

{ TNACustomEyes }

procedure TNACustomEyes.SetBounds(ALeft, ATop, AWidth, AHeight: integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  Bmp.SetSize(AWidth,AHeight);
  if not (csLoading in ComponentState) then //отакот работает, можно убрать FIsLoaded
  begin
    FirstTimer:=255;
    RePaint;
  end;
end;

procedure TNACustomEyes.SeTEyesColor(AValue: Tcolor);
begin
  if FEyesColor=AValue then Exit;
  FEyesColor:=AValue;
  if not (csLoading in ComponentState)
   then rePaint;
end;

procedure TNACustomEyes.SetViewEnabled(const AValue: Boolean);
begin
  if FViewEnabled=AValue then exit;
  FViewEnabled:=AValue;
    Timer.Enabled:=AValue;
  if AValue then FirstTimer:=255;
end;

procedure TNACustomEyes.SetAppleColor(AValue: TColor);
begin
  if FAppleColor=AValue then Exit;
  FAppleColor:=AValue;
  if not (csLoading in ComponentState)
   then RePaint;
end;

procedure TNACustomEyes.SetEavesColor(AValue: TColor);
begin
  if FEavesColor=AValue then Exit;
  FEavesColor:=AValue;
  if not (csLoading in ComponentState)
   then Repaint;
end;


procedure TNACustomEyes.SetFGColor(AValue: TColor);
begin
  if FFGColor=AValue then Exit;
  FFGColor:=AValue;
  if not (csLoading in ComponentState)
   then rePaint;
end;

procedure TNACustomEyes.SetBGColor(AValue: TColor);
begin
  if FBGColor=AValue then Exit;
  FBGColor:=AValue;
    if not (csLoading in ComponentState)
     then repaint;
end;

procedure TNACustomEyes.Repaint;
begin
  inherited Repaint;
  oldmx:=0;oldmy:=0;
  EyesOnTimer;
  Paint;
end;

procedure TNACustomEyes.Loaded;
begin
  inherited Loaded;
  Bmp.SetSize(Width, Height);
  FirstTimer:=255;
  EyesOnTimer;
//  RePaint;  //в 0.9.30 нормально, в 1.0.8, 1.0.10 ошибки сыпет. В винде нормально
  Timer.Interval:=100;
  Timer.Enabled:=true;

end;

procedure TNACustomEyes.OnTimer(Sender: TObject);
begin
 if FirstTimer=255 then
   begin
     OldMx:=0;
     OldMy:=0;
     EyesOnTimer;
     FirstTimer:=0;
     Paint;
     exit;
   end;
 if EyesOnTimer then
  Paint;
end;

function TNACustomEyes.EyesOnTimer:boolean;
var MLx,Rad,MLy, Cx, Cy: integer;
  x,y,F2,MLx2,MLy2:double;
begin
  //двигаем зрачками
  //Cx, Cy - коорд. центра глаза
 result:=false;
 Mx:=mouse.CursorPos.x;
 My:=mouse.CursorPos.y;
 if FirstTimer<>255 then
  if (abs(Mx-OldMx)<2) and (abs(My-OldMy)<2)
   then exit;
 oldMx:=Mx; OldMy:=My;
 result:=true;
 rad:=(height-2) div 2;
 F:=4*height; //Z - третье измерение, типа глаз - шар, смотрит вперед по Z

 F2:=sqr(F);
 //левый
 Cx:=Parent.Left+Left+height2;// центр
 Cy:=Parent.Top+Top+height2;
 MLx:=Mx-Cx; //мышь в координатах от центра
 MLy:=My-Cy;
 Mlx2:=sqr(MLx);
 MLy2:=sqr(MLy);
 //проецируем на окружность глаза из 3D по Z, 2 уравнения пересечения
 //прямой с окружностью в плоскостях ZX и ZY
 x:=(height2*MLx)/sqrt(F2+MLx2);
 y:=(height2*MLy)/sqrt(F2+MLy2);

 if sqr(x)+sqr(y)>sqr(rad-Dx)
  then
  begin
    Lx:=trunc(((rad-Dx)*x)/sqrt(sqr(y)+sqr(x)))+height2;
    Ly:=trunc(((rad-Dx)*y)/sqrt(sqr(x)+sqr(y)))+height2;
  end
  else
  begin
   Lx:=trunc(x)+height2;
   Ly:=trunc(y)+height2;
  end;
//правый
 Cx:=parent.left+Left+height+height2;// центр
// Cy:=Parent.Top+Top+height2;
 MLx:=Mx-Cx; //мышь в координатах от центра
// MLy:=My-Cy;
 Mlx2:=sqr(MLx);
// MLy2:=sqr(MLy);
 x:=(Height2*MLx)/sqrt(F2+MLx2);
// y:=(Height2*MLy)/sqrt(F2+MLy2);
 if sqr(x)+sqr(y)>sqr(rad-Dx)
  then
  begin
    Rx:=trunc(((rad-Dx)*x)/sqrt(sqr(y)+sqr(x)))+height+height2;
    Ry:=trunc(((rad-Dx)*y)/sqrt(sqr(y)+sqr(x)))+height2;
  end
  else
  begin
   Rx:=trunc(x)+height+height2;
   Ry:=trunc(y)+height2;
  end;
 //Ry:=Ly;
end;

constructor TNACustomEyes.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Parent:=AOwner as TWinControl;
  Bmp:=TBitmap.Create;
  Timer:=TTimer.Create(self);
  Timer.Enabled:=false;
  Timer.Interval:=100;
  Timer.OnTimer:=@OnTimer;
  FirstTimer:=0;
end;

destructor TNACustomEyes.Destroy;
begin
  Timer.Enabled:=false;
  FreeAndNil(Bmp);
  FreeAndNil(Timer);
  inherited Destroy;
end;



{ TNAEyes }

procedure TNAEyes.Paint;
var d3:integer;  FuckColor:TColor;
begin
    if not (Visible) then exit;
    inherited Paint;
  if Ly=0 then
   begin
     Ly:=Height div 2;
     Ry:=Ly;
     Lx:=Ly;
     Rx:=Height+Lx;
   end;


  //FuckColor - цвет границы для заливки век, чтобы ни с каким не совпадал
    if FFGColor+3<$7FFFFFFF then
    FuckColor:=FFGColor+1
    else
      FuckColor:=FFGColor-10;
   if FuckColor=FBGColor then FuckColor:=FuckColor+1;
   if FuckColor=FEyesColor then FuckColor:=FuckColor+1;
   if FuckColor=FBGColor then FuckColor:=FuckColor+1;

  Height5:=Height div 5;
  Height2:=Height div 2;
// рисуем неподвижную часть глаза (ну и очищаем картинку, рисуем Border)
  Bmp.Canvas.Pen.Color:=FBGColor;
  Bmp.Canvas.Brush.Color:=FBGColor;
  Bmp.Canvas.Brush.Style:=bsSolid;
  Bmp.Canvas.FillRect(0,0,Width,Height);
  if Border then
  begin
    Bmp.Canvas.Pen.Color:=clWhite;
    Bmp.Canvas.Rectangle(0,0,width,height);
    Bmp.Canvas.Pen.Color:=clBlack;
    Bmp.Canvas.Line(1,height-1,width-1,height-1);
    Bmp.Canvas.Line(width-1,height-1,width-1,1);
  end;
  Bmp.Canvas.Pen.Color:=FuckColor;
  Bmp.Canvas.Brush.Color:=FEyesColor;
  Bmp.Canvas.Brush.Style:=bsSolid;//с заливкой
  Bmp.Canvas.Ellipse(1, 1, height-1, height-1);
  Bmp.Canvas.Ellipse(height, 1, width-2, height-1);

  //дуги век
  Bmp.Canvas.Arc(1,height5,Height-1,Height-1,height-1, height2, 1,height2);
  Bmp.Canvas.Arc(height,height5,width-2,Height-1,width-2, height2, height,height2);

//---------- рисуем зрачок

  bmp.Canvas.Pen.Color:=clBlack;
  bmp.Canvas.Brush.Color:=FAppleColor;
  bmp.Canvas.Ellipse(Lx-Dx, Ly-Dx, Lx+Dx, Ly+Dx);
  bmp.Canvas.Ellipse(Rx-Dx, Ry-Dx, Rx+Dx, Ry+Dx);
  d3:=max(1,Dx div 3);
  bmp.Canvas.Brush.Color:=clBlack;
  bmp.Canvas.Ellipse(Lx-D3, Ly-D3, Lx+D3, Ly+D3);
  bmp.Canvas.Ellipse(Rx-D3, Ry-D3, Rx+D3, Ry+D3);

  //------------------------------

//-----------------еще раз факколором-
Bmp.Canvas.Pen.Color:=FuckColor;
Bmp.Canvas.Brush.Color:=FEyesColor;
Bmp.Canvas.Brush.Style:=bsClear;
Bmp.Canvas.Ellipse(1, 1, height-1, height-1);
Bmp.Canvas.Ellipse(height, 1, width-2, height-1);

//дуги век
Bmp.Canvas.Arc(1,height5,Height-1,Height-1,height-1, height2, 1,height2);
Bmp.Canvas.Arc(height,height5,width-2,Height-1,width-2, height2, height,height2);
//-------------заливка дуг
FloodFillLines(Bmp,FEavesColor,FuckColor,height2-1,2, 0,2,height-1,height2-1);
FloodFillLines(Bmp,FEavesColor,FuckColor,height+height2-1,2, height,2,width-1,height2-1);

//нормальным цветом
 Bmp.Canvas.Pen.Color:=FFGColor;
 Bmp.Canvas.Brush.Color:=FEyesColor;
 Bmp.Canvas.Brush.Style:=bsClear;//без заливки
 Bmp.Canvas.Ellipse(1, 1, height-1, height-1);
 Bmp.Canvas.Ellipse(height, 1, width-2, height-1);
 //дуги век
 Bmp.Canvas.Arc(1,height5,Height-1,Height-1,height-1, height2, 1,height2);
 Bmp.Canvas.Arc(height,height5,width-2,Height-1,width-2, height2, height,height2);
  //--------------------------------

  Canvas.Draw(0,0,Bmp);

end;

procedure TNAEyes.SetBounds(ALeft, ATop, AWidth, AHeight: integer);
begin
   if AWidth<>Width then
     AHeight:=trunc(AWidth/2)
   else
    if AHeight<>Height then
     AWidth:=AHeight*2;

  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
   Dx:=(height-2) div 5;
end;

constructor TNAEyes.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
//  Parent:=AOwner as TWinControl;

  Constraints.MinHeight:=16;
  Constraints.MinWidth:=32;
  FBGColor:=clBtnFace;
  FFGColor:=clBlack;
  FEyesColor:=$00E1E9EA;
  FAppleColor:=clSkyBlue;
  FEavesColor:=clSilver;//$00C2ECED;
  Dx:=4; //Dx=(height-2)/4 - радиус, Tx, Ty - координаты центра (height/2)
  Lx:=8; Ly:=8; // это для левого зрачка
  Rx:=24; Ry:=8;
  F:=16;//поставим по Z равным height
  OldMx:=0;//mouse.CursorPos.x+1;
  OldMy:=0;//mouse.CursorPos.y+1;
end;

destructor TNAEyes.Destroy;
begin

  inherited Destroy;
end;


end.
