unit ipaddr;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils {$IfDef WINDOWS}, winsock, IdStack, IdGlobal {$Endif}
  {$IfDef UNIX}, ipget {$Endif};

function GetLocalIP(lastOfAll: Boolean = true):string;
function GetNetwork(IP: String): String;
function GetNetwork: String;
function GetUserName: String;
function IsIPAddress(IP: String): Boolean;

implementation

{$IfDef WINDOWS}
function GetWinLocalIP : String;
Var WSAData: TWSAData;
    P: PHostEnt;
    Name: array[0..$FF] of Char;
    sl: TStringList;
    i, addrCount: Integer;
    addrList: TIdStackLocalAddressList;
Begin
  // ----- indy method ---------------
  GStack.IncUsage;
  Result:='127.0.0.1';
  addrList:=TIdStackLocalAddressList.Create;
  try
    GStack.GetLocalAddressList(addrList);
  finally
    GStack.DecUsage;
  end;
  sl:=TStringList.Create;
  addrCount:=addrList.Count-1;
  for i:=0 to addrCount do
  begin
    if (addrList.Addresses[i].IPVersion = Id_IPv4) then
    begin
      sl.Add(addrList.Addresses[i].IPAddress);
    end;
  end;
  addrList.Free;

  if (sl.Count = 0) then
  begin
    Exit(Result);
  end;

  // for debug
  try
    sl.SaveToFile('ips.txt');
  finally
  end;

  // take first or most common (192.168.)
  Result:=sl[0];
  for i:=1 to sl.Count-1 do
  begin
    // take most common local ip
    if (sl[i].Contains('192.168.')) then
    begin
      Result:=sl[i];
      Break;
    end;
  end;
  sl.Free;
  Exit(Result);
// ---------- end ----------------

// winapi method
  WSAStartup($0101, WSAData);
  GetHostName(Name, $FF);
  P := GetHostByName(Name);
    //// ---- all addresses -----
    //sl:=TStringList.Create;
    //i:=0;
    //while (P^.h_addr_list[i] <> nil) do
    //begin
    //  addr:=inet_ntoa(PInAddr(P^.h_addr_list[i])^);
    //  sl.Add(addr);
    //  inc(i);
    //end;
    //FreeAndNil(sl);
    //// ------------------------
  Result := inet_ntoa(PInAddr(P^.h_addr_list^)^);
  WSACleanup;
End;
{$Endif}

// lastOfAll = true - get last ip, otherwise first
function GetLocalIP(lastOfAll: Boolean = True): string;
var HostAddr: String; i, p, l: integer;
begin
  {$IfDef UNIX}
    //HostAddr:=GetIpList;
    HostAddr:=GetIpAddrList;
    l:=Length(HostAddr);
    if l = 0 then // address not found..
       Exit('127.0.0.1');
    if lastOfAll then
    begin
      i:=l;
      while (i>1) and (HostAddr[i]<>',') do dec(i);
      if i=1 then i:=0;
      HostAddr:=copy(HostAddr,i+1,l-i);
    end
    else
    begin
      i:=0;
      p:=Pos(',', HostAddr);
      // if p = 0 there is only one address
      if p <> 0 then // copy first ip
         HostAddr:=Copy(HostAddr, 1, p - 1);
      HostAddr:=Trim(HostAddr);
    end;

  {$Else}
    HostAddr:=GetWinLocalIP;
  {$Endif}
  Result:=HostAddr;
end;

function GetNetwork(IP: String): String;
var l, i: integer;
begin
  Result:=IP;
  if IP='' then Exit;
  l:=Length(IP);
  i:=l;
  while (IP[i]<>'.') and (i>1) do dec(i);
  if i=1 then i:=0;
  Result:=Copy(IP,1,i)+'255';
end;

function GetNetwork: String;
begin
  Result:=GetNetwork(GetLocalIP);
end;

function GetUserName: String;
begin
  {$IfDef WINDOWS}
  Result:=GetEnvironmentVariable('username');
  {$Else}
  Result:=GetEnvironmentVariable('USER');
  {$Endif}
end;

function IsIPAddress(IP: String): Boolean;
var parts: TStringArray;
    i: byte; num: LongInt;
    s: string;
begin
  Result:=false;
  if (string.IsNullOrEmpty(IP)) then Exit(Result);
  parts:=IP.Split('.');
  if (Length(parts) <> 4) then Exit(Result);
  for i:=1 to 4 do
  begin
    s:=parts[i];
    if not(TryStrToInt(s, num)) then
    begin
      Exit(Result);
    end;
  end;
  Result:=true;
end;


end.

