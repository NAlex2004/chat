unit ChatPopUp;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs,
   LMessages, LCLIntf, StdCtrls, ExtCtrls, ChatSettingsType, NAStringList, Types;

type

  { TPopUpForm }

  TPopUpForm = class(TForm)
    lUser: TLabel;
    lMsg: TLabel;
    Timer1: TTimer;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure ContextPopup(Sender: TObject; MousePos: TPoint;
        var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure MsgListMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Timer1Timer(Sender: TObject);
    procedure HandleRelease(var Msg: TLMessage); message CM_RELEASE;
  private
    { private declarations }
    UnreadCount: Integer;
    procedure SetMsg(Msg: String);
  public
    { public declarations }
    procedure ShowChatMsg(MainFrm: TForm; Usr: String; Msg: String);
    procedure IncreaseUnreadCount;
  end;

const DX = 7;
      DY = 25;
      WDY = 5;

//      MAXCHARS = 14;

var
  PopUpForm: TPopUpForm;
  TimeOut: Integer;
  MForm: TForm;
implementation

{$R *.lfm}

{ TPopUpForm }

procedure TPopUpForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  timer1.Enabled:=false;
end;

procedure TPopUpForm.ContextPopup(Sender: TObject; MousePos: TPoint;
    var Handled: Boolean);
begin
    Handled:=true;
end;

procedure TPopUpForm.FormCreate(Sender: TObject);
begin
  Top:=DY+PopUpsShown*Height;
  if PopUpsShown>0 then Top:=Top+WDY;
  Left:=Screen.Width-Width-DX;
  TimeOut:=Settings.MsgTime;
  Timer1.Enabled:=true;
  Inc(PopUpsShown);
  if Top+Height*2>Screen.Height-DY then // *2 т.к. добавляем нижнее окно
   begin
     Top:=DY;
     PopUpsShown:=0;
   end;
  UnreadCount:=1;
end;

procedure TPopUpForm.FormPaint(Sender: TObject);
begin
  Canvas.Rectangle(ClientRect);
end;

procedure TPopUpForm.MsgListMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //NeedAttension:=false;
  if Button=mbLeft then
    PostMessage(MForm.Handle, LM_SHOWMAINFORM, 0, 0)
    else
      NeedAttension:=False;
  PostMessage(Handle, CM_RELEASE, 0, 0);
end;

procedure TPopUpForm.Timer1Timer(Sender: TObject);
begin
  Dec(TimeOut);
  if TimeOut<0 then //Close;
   PostMessage(Handle, CM_RELEASE, PtrInt(Sender), 0);
end;

procedure TPopUpForm.HandleRelease(var Msg: TLMessage);
begin
  Timer1.Enabled:=false;
  //Free;
  //Dec(PopUpsShown);
  if Top=DY then
   PopUpsShown:=0;
  Release;
end;

procedure TPopUpForm.SetMsg(Msg: String);
var i, l: Integer; s: String; sl: TStringList;
begin
  lMsg.Caption:='';
  l:=Length(Msg);
  sl:=TStringList.Create;
  i:=1;
  repeat
   s:='';
   while (i<=l) and not(Msg[i] in [#10, #13]) do
   begin
     s:=s+Msg[i];
     inc(i);
   end;
   while (i<=l) and (Msg[i] in [#10, #13]) do inc(i);
   if s<>'' then
    TNAStringList.WordWrapPix(s, lMsg.Width, lMsg.Font.Style, lMsg.Font.Size, sl);
  until i>l;
  for i:=0 to sl.Count-1 do
   lMsg.Caption:=lMsg.Caption+sl.Strings[i]+' ';
  sl.Free;
end;

procedure TPopUpForm.ShowChatMsg(MainFrm: TForm; Usr: String; Msg: String);
begin
  Left:=Screen.Width-Width-DX;
  lUser.Caption:=Usr;
  SetMsg(Msg);
  ShowInTaskBar:=stNever;
  MForm:=MainFrm;
  Visible:=true;
  //Show;
end;

procedure TPopUpForm.IncreaseUnreadCount;
begin
  Inc(UnreadCount);
  lMsg.Caption:=IntToStr(UnreadCount);
end;

end.

