unit logviewer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  LCLType, LazUTF8;

type

  { TLogForm }

  TLogForm = class(TForm)
    bFind: TButton;
    eFind: TEdit;
    LogView: TMemo;
    procedure bFindClick(Sender: TObject);
    procedure eFindChange(Sender: TObject);
    procedure eFindKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  LogForm: TLogForm;
  FSelPos: Integer;
implementation

{$R *.lfm}

{ TLogForm }

procedure TLogForm.eFindKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_RETURN: bFindClick(Sender);
    VK_ESCAPE: Close;
  end;
end;

procedure TLogForm.FormActivate(Sender: TObject);
begin
  FSelPos:=0;
end;

procedure TLogForm.bFindClick(Sender: TObject);
begin

 FSelPos:=UTF8Pos(UTF8LowerCase(eFind.Text), UTF8LowerCase(LogView.Text), FSelPos+1);
  //  LogView.SelStart+LogView.SelLength+1);

 if FSelPos>0 then
 begin
   LogView.SelStart:=FSelPos-1;
   LogView.SelLength:=UTF8Length(eFind.Text);
   FSelPos:=FSelPos+UTF8Length(eFind.Text);
   {$ifdef linux}
    LogView.SetFocus;
   {$endif}
  end;
end;

procedure TLogForm.eFindChange(Sender: TObject);
begin
  FSelPos:=0;
end;

end.
