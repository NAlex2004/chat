unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, StdCtrls, Menus, IdUDPServer, IdUDPClient, ChatPopUp, ChatSettings,
  ChatSettingsType, ipaddr, ChatTabSheet, uLocale, IdSocketHandle,
  IdGlobal, types, LCLType, LCLIntf, LMessages, logviewer, LResources
  {$IfDef LINUX}, x, xlib, xwin{$EndIf}, IdComponent;

Const SETFILE = 'chatset.ini';
      LOGFILE = 'history.log';
      HEAD_ONLINE = #1+#1+#1;
      HEAD_OFFLINE = #1+#1+#2;
      HEAD_BROADCAST = #2+#2+#2;
      HEAD_PRIVATE = #2+#2+#1;
      HEAD_RENAME = #1+#1+#3;
      END_NAME = #3+#3+#3;
      OFFLINECOLOR = clGray;
      MSGONLINE = '#ONLINE#';
      MSGOFFLINE = '#OFFLINE#';

type

  { TMainForm }

  TMainForm = class(TForm)
    ChatPages: TPageControl;
    Label1: TLabel;
    miUsersListClear: TMenuItem;
    miLogView: TMenuItem;
    miClearLog: TMenuItem;
    TestTimer: TTimer;
    UsersPopupMenu: TPopupMenu;
    tmiShow: TMenuItem;
    tmiExit: TMenuItem;
    miSubSettings: TMenuItem;
    miSettings: TMenuItem;
    TrayPopUp: TPopupMenu;
    Splitter1: TSplitter;
    Client: TIdUDPClient;
    Server: TIdUDPServer;
    MainMenu: TMainMenu;
    miChat: TMenuItem;
    miCloseTab: TMenuItem;
    miFile: TMenuItem;
    miExit: TMenuItem;
    OnlineTimer: TTimer;
    TrayIcon: TTrayIcon;
    Users: TListBox;
    UserTab: TTabSheet;
    UserPages: TPageControl;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure FormWindowStateChange(Sender: TObject);
    procedure miClearLogClick(Sender: TObject);
    procedure miLogViewClick(Sender: TObject);
    procedure miUsersListClearClick(Sender: TObject);
    procedure TestTimerTimer(Sender: TObject);
    procedure tmiExitClick(Sender: TObject);
    procedure tmiShowClick(Sender: TObject);
    procedure miCloseTabClick(Sender: TObject);
    procedure miExitClick(Sender: TObject);
    procedure miSubSettingsClick(Sender: TObject);
    procedure OnlineTimerTimer(Sender: TObject);
    procedure ServerUDPRead(AThread: TIdUDPListenerThread; AData: TIdBytes;
      ABinding: TIdSocketHandle);
    procedure TrayIconClick(Sender: TObject);
    procedure TrayPopUpPopup(Sender: TObject);
    procedure UsersDblClick(Sender: TObject);
    procedure UsersDrawItem(Control: TWinControl; Index: Integer; ARect: TRect;
      State: TOwnerDrawState);
    procedure UsersShowHint(Sender: TObject; HintInfo: PHintInfo);
  private
    { private declarations }
    Activated: Boolean;
    procedure OpenChatSheet(var CItem: TChatItem);
    procedure ShowIncomminMessage(CItem: TChatItem; FromName: String; FromIP: String;
                                   Msg: String);
    procedure ShowPrivateMessage(FromName: String; FromIP: String; Msg: String);
    procedure ShowSystemMessage(FromName: String; Msg: String);
    procedure UserOnline(UserNick: String; UserIP: String; Msg: String);
    procedure UserOffline(UserNick: String; UserIP: String; Msg: String);
    procedure UserRenamed(UserNick: String; UserIP: String; Msg: String);
    procedure FreeUsersObjects;
    procedure CloseChatSheet(Sender: TObject);
    function CreatePopUp: TPopUpForm;
    procedure OpenLogFile;
    procedure WriteLogFile(Msg: String);
    procedure HandleShowMainFormMsg(var Msg: TLMessage); message LM_SHOWMAINFORM;
  public
    { public declarations }
    function ReadSettings: TChatSettings;
    procedure FillCurrentSettings(var cs: TChatSettings);
    procedure WriteSettings(cs: TChatSettings);
    procedure ApplySettings(cs: TChatSettings);
    procedure SendChatMessage(Sender: TObject; Msg: string);
    procedure SendSystemMessage(Head: String; Msg: string);
    function GetUserIndex(UserNick: String; UserIP: String): Integer;
    function GetUserIndexByIP(UserIP: String): Integer;
    function AddUserToList(UserNick: String; UserIP: String): Integer;
    function GetOrAddChatItem(UserNick: String; UserIP: String): TChatItem;
  end;

var
  MainForm: TMainForm;
//  Settings: TChatSettings;
  MainChat: TChatItem;//TNAChatTabSheet;
  ReallyClose: Boolean = false;
  LocalIP: string;
  Exiting, StartError: Boolean;
  PopUpFrm: TPopUpForm;
  FLog: TextFile;
  OldSize: TRect;
  IconSwitch: Boolean;
  Pict: TPicture;
  UnreadMessagesForm: TPopUpForm;
  {$IfDef Linux}
   dsp: PDisplay; win: PWindow; actwin: TWindow;
  {$EndIf}
implementation

{$R *.lfm}

function FileOpened(var F: TextFile): boolean;
begin
  case TTextRec(F).Mode of fmInput, fmOutput, fmInOut:
    Result:=true;
  else
    Result:=false;
  end;
end;

{ TMainForm }

procedure TMainForm.miExitClick(Sender: TObject);
begin
  ReallyClose:=true;
  Close;
end;

procedure TMainForm.miSubSettingsClick(Sender: TObject);
var OldNick: String;
begin
  OldNick:=Settings.Nick;
  if SetupForm.ShowChatSettings(Settings) then
  begin
   //Если переименовались
   if OldNick<>Settings.Nick then
     begin
      Client.BroadcastEnabled:=true;
      Client.Send(HEAD_RENAME+OldNick+END_NAME+Settings.Nick+' ', IndyTextEncoding_UTF8, IndyTextEncoding_UTF8);
     end;
   Settings.Left:=Left;
   Settings.Top:=Top;
   Settings.Height:=Height;
   Settings.Width:=Width;
   WriteSettings(Settings);
   ApplySettings(Settings);
  end;
end;

procedure TMainForm.OnlineTimerTimer(Sender: TObject);
var i: Integer;
    item: TChatItem;
begin
  Dec(MainChat.TimeOut);
  if MainChat.TimeOut<=1 then
  begin
   SendSystemMessage(HEAD_ONLINE, Settings.Nick);
   MainChat.TimeOut:=ITEMTIMEOUT;
   Users.Invalidate;
  end;
  for i:=0 to Users.Items.Count-1 do
  begin
   item:=TChatItem(Users.Items.Objects[i]);
   if (Assigned(item)) then
    if item.TimeOut>0 then Dec(item.TimeOut);
  end;
  if NeedAttension then
  begin
   if Assigned(Pict) then
    begin
      IconSwitch:=not(IconSwitch);
      if IconSwitch then
        Pict.LoadFromLazarusResource('online')
        else
          Pict.LoadFromLazarusResource('message');
      TrayIcon.Icon.Assign(Pict.Icon);
      TrayIcon.InternalUpdate;
    end;
  end
  else
   if not(IconSwitch) then
    begin
      IconSwitch:=true;
      if Assigned(Pict) then
       begin
        Pict.LoadFromLazarusResource('online');
        TrayIcon.Icon.Assign(Pict.Icon);
        TrayIcon.InternalUpdate;
       end;
    end;
end;

procedure TMainForm.ServerUDPRead(AThread: TIdUDPListenerThread;
  AData: TIdBytes; ABinding: TIdSocketHandle);
var s: PString; i, len: Integer; Head, FromNick, Msg: String;
begin
  //------------
  //if ABinding.PeerIP=LocalIP then Exit;
  if Exiting then Exit;
  len:=Length(AData);
  if len<Length(HEAD_BROADCAST+END_NAME) then Exit;
  s:=@AData;
  Head:=s^[1]+s^[2]+s^[3];
  i:=4;
  while (i<len-2)and((s^[i]+s^[i+1]+s^[i+2])<>END_NAME) do inc(i);
  FromNick:=Copy(s^, 4, i-4);
  Msg:=Copy(s^, i+3, len-i-2);
  case Head of
   HEAD_ONLINE:begin
       UserOnline(FromNick, ABinding.PeerIP, Msg);
     end;
   HEAD_OFFLINE:begin
       UserOffline(FromNick, ABinding.PeerIP, Msg);
     end;
   HEAD_PRIVATE:begin
       ShowPrivateMessage(FromNick, ABinding.PeerIP, Msg);
     end;
   HEAD_BROADCAST:begin
       if ABinding.PeerIP=LocalIP then exit;
       ShowIncomminMessage(MainChat, FromNick, ABinding.PeerIP, Msg);
     end;
   HEAD_RENAME:begin
       UserRenamed(FromNick, ABinding.PeerIP, Utf8ToAnsi(Msg));
     end;
  end;
end;

procedure TMainForm.TrayIconClick(Sender: TObject);
begin
  //if ShowInTaskBar=stNever then
  if NeedAttension and Assigned(UnreadMessagesForm) then
  begin
     UnreadMessagesForm.Close;
     FreeAndNil(UnreadMessagesForm);
  end;
  NeedAttension:=false;
  if not(Visible) then
  begin
    ShowInTaskBar:=stDefault;
    if Settings.Maximized then WindowState:=wsMaximized
     else WindowState:=wsNormal;
    Show;
  end
   else
  begin
   ShowInTaskBar:=stNever;
   Hide;
  end;
end;

procedure TMainForm.TrayPopUpPopup(Sender: TObject);
begin
  NeedAttension:=false;
end;

procedure TMainForm.UsersDblClick(Sender: TObject);
var CItem: TChatItem;
begin
  if Users.ItemIndex<0 then Exit;
  with Users do
  begin
    CItem:=TChatItem(Items.Objects[ItemIndex]);
  end;
  if not(Assigned(CItem.Sheet)) then OpenChatSheet(CItem);
  ChatPages.ActivePage:=CItem.Sheet;
end;

procedure TMainForm.UsersDrawItem(Control: TWinControl; Index: Integer;
  ARect: TRect; State: TOwnerDrawState);
var RColor, BColor:TColor; i, d, c: Integer;
begin
  //if not Assigned((Control as TListBox).Items.Objects[Index]) then
  //begin
  //   (Control as TListBox).Items.Delete(Index);
  //   Exit;
  //end;

  with Control as TListBox do
  begin
    if odSelected in State then
    begin
      BColor:=clHighlight;
      Canvas.Brush.Color:=clHighlight;
    end
      else
      begin
       Canvas.Brush.Color:=Control.Color;
       BColor:=Control.Color;
      end;

    Canvas.FillRect(Arect);

    //Canvas.Brush.Color:=Control.Color;
//    Canvas.Font:=Font;
    Canvas.Font.Color:=OFFLINECOLOR;
    RColor:=clRed;

    if Assigned(Items.Objects[Index]) then
      if TChatItem(Items.Objects[Index]).TimeOut<=0 then
      begin
         Canvas.Font.Color:=OFFLINECOLOR;
         RColor:=clRed;
      end
        else
        begin
         Canvas.Font.Color:=Font.Color;
         RColor:=clGreen;
        end;
    if odSelected in State then
      Canvas.Font.Color:=clHighlightText;
    Canvas.Brush.Color:=RColor;
    Canvas.Pen.Color:=RColor;//Font.Color;
    d:=((ARect.Bottom-ARect.Top) div 3);
    c:=(ARect.Bottom-ARect.Top) div 2;
    for i:=0 to d-1 do
      Canvas.Ellipse(ARect.Left+c-i, ARect.Top+c-i, ARect.Left+c+i, ARect.Top+c+i);
    Canvas.Pen.Color:=Canvas.Font.Color;
    Canvas.Ellipse(ARect.Left+c-d, ARect.Top+c-d, ARect.Left+c+d, ARect.Top+c+d);
    Canvas.Brush.Color:=BColor;
    Canvas.TextOut(ARect.Left+d*4, ARect.Top, Items.Strings[Index]);
  end;
end;

procedure TMainForm.UsersShowHint(Sender: TObject; HintInfo: PHintInfo);
var
    i: Integer;
    item: TChatItem;
begin
    i:=Users.ItemAtPos(HintInfo^.CursorPos, True);
    if (i > -1) then
    begin
      item:=Users.Items.Objects[i] as TChatItem;
      if Assigned(item) then
      begin
        HintInfo^.HintStr:=item.IP;
      end;
    end;
end;

procedure TMainForm.OpenChatSheet(var CItem: TChatItem);
begin
  //------ открыть таб для имеющегося ChatItem
  CItem.Sheet:=TNAChatTabSheet.Create(ChatPages);
  with CItem.Sheet do
  begin
    Parent:=ChatPages;
    Color:=Settings.MainColor;
    ChatItem:=CItem;
    Caption:=CItem.Nick;
    OnSendClick:=@SendChatMessage;
    OnCloseClick:=@CloseChatSheet;
    OnMouseMove:=Self.OnMouseMove;
    Msg.OnMouseMove:=Self.OnMouseMove;
    TextList.OnMouseMove:=Self.OnMouseMove;
    Msg.Color:=Settings.MainColor;
    Msg.Font.Color:=Settings.MainFontColor;
    TextList.Color:=Settings.MainColor;
    TextList.Font.Color:=Settings.MainFontColor;
    FromMsgColor:=Settings.MainFontColor;
    OutMsgColor:=Settings.MainFontColor;
    FromColor:=Settings.IncNickColor;
    OutColor:=Settings.OutNickColor;
  end;
end;

procedure TMainForm.ShowIncomminMessage(CItem: TChatItem; FromName: String; FromIP: String;
  Msg: String);
var FocusedControl: TWinControl; PopColor: TColor; ActForm: TForm; ToNick: String;
begin
  {$IfDef Linux}
   dsp:=XOpenDisplay(nil);
   GetActiveWindow(dsp, win);
   actwin:=win[0];
   XFree(win);
  // PostMessage(Handle, LM_GETACTWIN, 0, 0);
  {$EndIf}
   FocusedControl:=Screen.ActiveControl;
   ActForm:=Screen.ActiveForm;

  PopUpFrm:=CreatePopUp;
  with PopUpFrm do
  begin
    if CItem=MainChat then
      begin
        PopColor:=Settings.MsgFontColor;
        ToNick:=SAll;
      end
      else
      begin
       PopColor:=Settings.MsgPrivFontColor;
       ToNick:=Settings.Nick;
      end;
    lUser.Font.Color:=PopColor;
    lMsg.Font.Color:=PopColor;
    ShowChatMsg(MainForm, FromName, Msg);
  end;

  //-------- окно внизу для невнимательных ------
  if not(NeedAttension) and not(Visible) then
  begin
     UnreadMessagesForm:=TPopUpForm.Create(self);
     UnreadMessagesForm.ShowInTaskBar:=stNever;
     UnreadMessagesForm.Timer1.Enabled:=false;
     UnreadMessagesForm.Width:=150;
     UnreadMessagesForm.Height:=46;
     UnreadMessagesForm.Top:=Screen.Height - UnreadMessagesForm.Height - DY - 16;
     UnreadMessagesForm.Left:=Screen.Width-Width-DX;
     UnreadMessagesForm.Color:=clYellow;
     UnreadMessagesForm.ShowChatMsg(MainForm, 'Новых сообщений:', '1');
     Dec(PopUpsShown);
  end
  else
      if Assigned(UnreadMessagesForm) then
      begin
        UnreadMessagesForm.IncreaseUnreadCount;
      end;

  //---------------------------------------------

  if not(Assigned(CItem.Sheet)) then
    OpenChatSheet(CItem);
  CItem.Sheet.AddIncommingMessage(FromName, Msg);

  {$IfDef Linux}
   ActivateWindow(dsp, actwin);
   //ActivateWindow(dsp, actwin);
   XCloseDisplay(dsp);
  //   PostMessage(Handle, LM_SETACTWIN, 0, 0);
  {$Else}
  if (ActForm=MainForm) or (ActForm=LogForm) or (ActForm=SetupForm) then
   //if WindowState<>wsMinimized then
   if Visible then
    FocusedControl.SetFocus;
  {$EndIf}
  if Settings.UseLog then
   WriteLogFile('['+DateToStr(Date)+' '+TimeToStr(Time)+']  '+Utf8ToAnsi(FromName)+
    ' -> '+Utf8ToAnsi(ToNick)+' : '+Utf8ToAnsi(Msg));
  NeedAttension:=true;
end;

procedure TMainForm.ShowPrivateMessage(FromName: String; FromIP: String;
  Msg: String);
var CItem: TChatItem; i: Integer;
begin
  //find citem and OpenChatSheet();
  //i:=GetUserIndex(FromName, FromIP);
  //if i<0 then
  //  i:=AddUserToList(FromName, FromIP);
  //CItem:=TChatItem(Users.Items.Objects[i]);
  CItem:=GetOrAddChatItem(FromName, FromIP);
  if (CItem <> nil) then
  begin
    if not(Assigned(CItem.Sheet)) then
       OpenChatSheet(CItem);
    ShowIncomminMessage(CItem, FromName, FromIP, Msg);
  end;
end;

procedure TMainForm.ShowSystemMessage(FromName: String; Msg: String);
var FocusedControl: TWinControl; AForm: TForm;
begin
  //----------
  {$IfDef Linux}

   dsp:=XOpenDisplay(nil);
   GetActiveWindow(dsp, win);
   actwin:=win[0];
   xfree(win);
  //  PostMessage(Handle, LM_GETACTWIN, 0, 0);
  {$EndIf}
  FocusedControl:=Screen.ActiveControl;
  AForm:=Screen.ActiveForm;

  PopUpFrm:=CreatePopUp;
  with PopUpFrm do
  begin
    lUser.Font.Color:=STATUSMSGCOLOR;
    lMsg.Font.Color:=STATUSMSGCOLOR;
    lUser.Font.Style:=[fsBold, fsItalic];
    lMsg.Font.Style:=[fsItalic];
    ShowChatMsg(MainForm, FromName, Msg);
  end;

   MainChat.Sheet.AddStatusMessage(FromName, Msg);
   label1.Caption:='.';

  {$IfDef Linux}
   ActivateWindow(dsp, actwin);
   //ActivateWindow(dsp, actwin);
   XCloseDisplay(dsp);
   //  PostMessage(Handle, LM_SETACTWIN, 0, 0);
  {$Else}
   if (AForm=MainForm) or (AForm=LogForm) or (AForm=SetupForm) then
    //if WindowState<>wsMinimized then
    if Visible then
     FocusedControl.SetFocus;
  {$EndIf}
end;

procedure TMainForm.UserOnline(UserNick: String; UserIP: String; Msg: String);
var i: Integer;
    chatItem: TChatItem;
begin
  if UserIP<>LocalIP then
  begin
    //i:=GetUserIndex(UserNick, UserIP);
    //if i<0 then
    //  i:=AddUserToList(UserNick, UserIP);
    //TChatItem(Users.Items.Objects[i]).TimeOut:=ITEMTIMEOUT;
    chatItem:=GetOrAddChatItem(UserNick, UserIP);
    if (chatItem <> nil) then
       chatItem.TimeOut:=ITEMTIMEOUT;
  end;

  if Utf8ToAnsi(Msg)=MSGONLINE then //открытие программы
  begin
  // MainChat.Sheet.AddStatusMessage(UserNick, SUserOnline);
   ShowSystemMessage(UserNick, SUserOnline);
  end;

end;

procedure TMainForm.UserOffline(UserNick: String; UserIP: String; Msg: String);
var i: Integer;
begin
  i:=GetUserIndex(UserNick, UserIP);
  if i>=0 then
  begin
    TChatItem(Users.Items.Objects[i]).TimeOut:=0;
    Users.Invalidate;
  end;
  if Utf8ToAnsi(Msg)=MSGOFFLINE then
  begin
  // MainChat.Sheet.AddStatusMessage(UserNick, SUserOffline);
   ShowSystemMessage(UserNick, SUserOffline);
  end;
end;

procedure TMainForm.UserRenamed(UserNick: String; UserIP: String; Msg: String);
var i: Integer;
    FullMsg: String;
begin
  if UserIP<>LocalIP then
    begin
      i:=GetUserIndex(UserNick, UserIP);
      if i>=0 then
       begin
         Users.Items.Strings[i]:=Msg;
         TChatItem(Users.Items.Objects[i]).Nick:=Msg;
         if Assigned(TChatItem(Users.Items.Objects[i]).Sheet) then
          TChatItem(Users.Items.Objects[i]).Sheet.Caption:=Msg;
       end;
    end;
  FullMsg:=SUserRenamed + Msg;
  ShowSystemMessage(UserNick, FullMsg);
end;

procedure TMainForm.FreeUsersObjects;
var i: Integer;
begin
  for i:=0 to Users.Items.Count-1 do
   if Assigned(Users.Items.Objects[i]) then
     TChatItem(Users.Items.Objects[i]).Free;
end;

procedure TMainForm.CloseChatSheet(Sender: TObject);
begin
  with Sender as TNAChatTabSheet do
  begin
    ChatItem.Sheet:=nil;
    PostMessage(Handle, CM_RELEASE, 0, 0);
  end;
end;

function TMainForm.CreatePopUp: TPopUpForm;
var pf: TPopUpForm;
begin
  pf:=TPopUpForm.Create(self);
  with pf do
  begin
    Color:=Settings.MsgColor;
    lUser.Font.Color:=Settings.MsgFontColor;
    lMsg.Font.Color:=Settings.MsgFontColor;
    ShowInTaskBar:=stNever;
  end;
  Result:=pf;
end;

procedure TMainForm.OpenLogFile;
begin
  if FileOpened(FLog) then Exit;
  try
   if FileExists(LOGFILE) then
     Append(FLog)
    else Rewrite(FLog);
  except
    ShowMessage(SLogError);
    Settings.UseLog:=false;
  end;
end;

procedure TMainForm.WriteLogFile(Msg: String);
begin
  if not(Settings.UseLog) or not(FileOpened(FLog)) then Exit;
  WriteLn(FLog, Msg);
end;

procedure TMainForm.HandleShowMainFormMsg(var Msg: TLMessage);
begin
  ShowInTaskBar:=stDefault;
  WindowState:=wsNormal;
  Show;
end;

function TMainForm.ReadSettings: TChatSettings;
var cs: TChatSettings;
    sl: TStringList;
    i, c, j, t: Integer;
    col: TColor;

  procedure SetMainDefault;
  begin
    cs.Left:=Left;
    cs.Top:=Top;
    cs.Width:=Width;
    cs.Height:=Height;
    cs.Nick:=ipaddr.GetUserName;
    cs.Maximized:=false;
  end;

begin
  SetDefault(cs);
  SetMainDefault;
  if FileExists(SETFILE) then
  begin
    sl:=TStringList.Create;
    sl.LoadFromFile(SETFILE);
    c:=sl.Count;
    if c>0 then
    begin
      i:=0;
      while i<c do
      begin
        case UpperCase(sl.Names[i]) of
         'MSGTIME':begin
                    val(sl.ValueFromIndex[i], t, j);
                    if j=0 then
                      cs.MsgTime:=t;
                   end;
         'MSGCOLOR': begin
                      col:=StringToColor(sl.ValueFromIndex[i]);
                      if col<>clNone then
                        cs.MsgColor:=col;
                     end;
         'MSGFONTCOLOR':begin
                         col:=StringToColor(sl.ValueFromIndex[i]);
                         if col<>clNone then
                           cs.MsgFontColor:=col;
                        end;
         'MSGPRIVFONTCOLOR':begin
                             col:=StringToColor(sl.ValueFromIndex[i]);
                             if col<>clNone then
                               cs.MsgPrivFontColor:=col;
                            end;
         'MAINCOLOR': begin
                      col:=StringToColor(sl.ValueFromIndex[i]);
                      if col<>clNone then
                        cs.MainColor:=col;
                     end;
         'MAINFONTCOLOR':begin
                         col:=StringToColor(sl.ValueFromIndex[i]);
                         if col<>clNone then
                           cs.MainFontColor:=col;
                        end;
         'INCNICKCOLOR':begin
                         col:=StringToColor(sl.ValueFromIndex[i]);
                         if col<>clNone then
                           cs.IncNickColor:=col;
                        end;
         'OUTNICKCOLOR':begin
                         col:=StringToColor(sl.ValueFromIndex[i]);
                         if col<>clNone then
                           cs.OutNickColor:=col;
                        end;
         'LEFT':begin
                 val(sl.ValueFromIndex[i], t, j);
                 if j=0 then
                   cs.Left:=t;
                end;
         'TOP':begin
                val(sl.ValueFromIndex[i], t, j);
                if j=0 then
                 cs.Top:=t;
               end;
         'WIDTH':begin
                  val(sl.ValueFromIndex[i], t, j);
                  if j=0 then
                    cs.Width:=t;
                 end;
         'HEIGHT':begin
                   val(sl.ValueFromIndex[i], t, j);
                   if j=0 then
                     cs.Height:=t;
                  end;
         'NICK': cs.Nick:=sl.ValueFromIndex[i];
         'PORT': begin
                   val(sl.ValueFromIndex[i], t, j);
                   if j=0 then
                     cs.Port:=t;
                 end;
         'TIMETOOFFLINE': begin
                   val(sl.ValueFromIndex[i], t, j);
                   if j=0 then
                     cs.TimeToOffline:=t;
                 end;
         'USELOG':begin
                    try
                      cs.UseLog:=StrToBool(sl.ValueFromIndex[i]);
                    Except on EConvertError do
                        cs.UseLog:=false;
                    end;
                 end;
         'MAXIMIZED':begin
                        try
                           cs.Maximized:=StrToBool(sl.ValueFromIndex[i]);
                        Except on EConvertError do
                           cs.Maximized:=false;
                        end;
                     end;
         'USELASTIP':begin
                        try
                           cs.UseLastIP:=StrToBool(sl.ValueFromIndex[i]);
                        Except on EConvertError do
                           cs.UseLastIP:=true;
                        end;
                      end;
         'USECUSTOMIP':begin
                        try
                           cs.UseCustomIP:=StrToBool(sl.ValueFromIndex[i]);
                        Except on EConvertError do
                           cs.UseCustomIP:=false;
                        end;
                      end;
         'CUSTOMIP': begin
                      cs.CustomIP:=sl.ValueFromIndex[i];
                     end;
         'STARTHIDDEN':begin
                        try
                           cs.StartHidden:=StrToBool(sl.ValueFromIndex[i]);
                        Except on EConvertError do
                           cs.StartHidden:=true;
                        end;
                      end;
        end;
        inc(i);
      end;
    end;
    sl.Free;
  end;
  OldSize.Left:=cs.Left;
  OldSize.Top:=cs.Top;
  OldSize.Right:=cs.Left+cs.Width;
  OldSize.Bottom:=cs.Top+cs.Height;
  if cs.TimeToOffline<=2 then cs.TimeToOffline:=3;
  if (cs.TimeToOffline > MINTIMEOUT) then
    ITEMTIMEOUT:=cs.TimeToOffline
    else
      ITEMTIMEOUT:=MINTIMEOUT;
  //WriteSettings(cs);
  Result:=cs;
end;

procedure TMainForm.FillCurrentSettings(var cs: TChatSettings);
begin
  with MainForm do
  begin
    cs.Maximized:=(WindowState=wsMaximized);
    if not(cs.Maximized) then
    begin
      cs.Left:=Left;
      cs.Top:=Top;
      cs.Width:=Width;
      cs.Height:=Height;
    end
     else
     begin
       cs.Left:=OldSize.Left;
       cs.Top:=OldSize.Top;
       cs.Width:=OldSize.Right-OldSize.Left;
       cs.Height:=OldSize.Bottom-OldSize.Top;
     end;
    cs.MainColor:=Users.Color;
    cs.MainFontColor:=Users.Font.Color;
  end;
end;

procedure TMainForm.WriteSettings(cs: TChatSettings);
var sl: TStringList;
begin
  sl:=TStringList.Create;
  sl.Add('NICK='+cs.Nick);
  sl.Add('MAINCOLOR='+ColorToString(cs.MainColor));
  sl.Add('MAINFONTCOLOR='+ColorToString(cs.MainFontColor));
  sl.Add('MSGCOLOR='+ColorToString(cs.MsgColor));
  sl.Add('MSGFONTCOLOR='+ColorToString(cs.MsgFontColor));
  sl.Add('MSGPRIVFONTCOLOR='+ColorToString(cs.MsgPrivFontColor));
  sl.Add('INCNICKCOLOR='+ColorToString(cs.IncNickColor));
  sl.Add('OUTNICKCOLOR='+ColorToString(cs.OutNickColor));
  sl.Add('LEFT='+IntToStr(cs.Left));
  sl.Add('TOP='+IntToStr(cs.Top));
  sl.Add('WIDTH='+IntToStr(cs.Width));
  sl.Add('HEIGHT='+IntToStr(cs.Height));
  sl.Add('MSGTIME='+IntToStr(cs.MsgTime));
  sl.Add('PORT='+IntToStr(cs.Port));
  sl.Add('TIMETOOFFLINE='+IntToStr(cs.TimeToOffline));
  sl.Add('USELOG='+BoolToStr(cs.UseLog));
  sl.Add('MAXIMIZED='+BoolToStr(cs.Maximized));
  sl.Add('USELASTIP='+BoolToStr(cs.UseLastIP));
  sl.Add('USECUSTOMIP='+BoolToStr(cs.UseCustomIP));
  sl.Add('CUSTOMIP='+cs.CustomIP);
  sl.Add('STARTHIDDEN='+BoolToStr(cs.StartHidden));
  if ((not string.IsNullOrEmpty(LocalIP)) and (MainChat <> nil)) then
  begin
    sl.Add('Debug_CurrentIP='+LocalIP);
    sl.Add('Debug_Network='+MainChat.IP);
  end;
  sl.SaveToFile(SETFILE);
  sl.Free;
end;

procedure TMainForm.ApplySettings(cs: TChatSettings);
var i, c: Integer; m: Boolean;
begin
 { with PopUpForm do
  begin
 //   TestTimer.Interval:=cs.MsgTime*1000;
    MsgList.Color:=cs.MsgColor;
    MsgList.Font.Color:=cs.MsgFontColor;
  end;
  }
  with MainForm do
  begin
    m:=cs.Maximized;
    cs.Maximized:=false;
{    Left:=cs.Left;
    Top:=cs.Top;
    Width:=cs.Width;
    Height:=cs.Height;}
    SetBounds(cs.Left, cs.Top, cs.Width, cs.Height);
    cs.Maximized:=m;
    if cs.Maximized then
     WindowState:=wsMaximized;
    Users.Color:=cs.MainColor;
    Users.Font.Color:=cs.MainFontColor;
    Caption:=cs.Nick;
  end;
  with ChatPages do
  begin
    c:=PageCount;
    for i:=0 to c-1 do
     with (Pages[i]) as TNAChatTabSheet do
     begin
       Color:=cs.MainColor;
       Msg.Color:=cs.MainColor;
       Msg.Font.Color:=cs.MainFontColor;
       TextList.Color:=cs.MainColor;
       TextList.Font.Color:=cs.MainFontColor;
       FromMsgColor:=cs.MainFontColor;
       OutMsgColor:=cs.MainFontColor;
       FromColor:=cs.IncNickColor;
       OutColor:=cs.OutNickColor;
     end;
  end;
  if (cs.TimeToOffline > MINTIMEOUT) then
    ITEMTIMEOUT:=cs.TimeToOffline
    else
      ITEMTIMEOUT:=MINTIMEOUT;
  //IP
  if (cs.UseCustomIP and IsIPAddress(cs.CustomIP)) then
  begin
    LocalIP:=cs.CustomIP;
    MainChat.IP:=GetNetwork(LocalIP);
    Client.Host:=MainChat.IP;
    Client.Active:=true;
  end;
  //PORT
  Server.DefaultPort:=cs.Port;
  Client.Port:=cs.Port;
  TrayIcon.Hint:=SChatHint+Settings.Nick;
  Application.Title:=SChatHint+Settings.Nick;
  if Activated then
   if Settings.UseLog then OpenLogFile;
end;

procedure TMainForm.SendChatMessage(Sender: TObject; Msg: string);
var Head, ToNick: String;
begin
  try
    if not(Client.Active) then
      Client.Active:=true;
    Client.Host:=TNAChatTabSheet(Sender).ChatItem.IP;
    if TNAChatTabSheet(Sender).ChatItem = MainChat then
      begin
       Client.BroadcastEnabled:=true;
       Head:=HEAD_BROADCAST;
       ToNick:=SAll;
      end
      else
      begin
       Client.BroadcastEnabled:=false;
       Head:=HEAD_PRIVATE;
       ToNick:=TNAChatTabSheet(Sender).ChatItem.Nick;
      end;

     Client.Send(Head+Settings.Nick+END_NAME+Msg+' ', IndyTextEncoding_UTF8, IndyTextEncoding_UTF8);
     if Settings.UseLog then
      WriteLogFile('['+DateToStr(Date)+' '+TimeToStr(Time)+']  '+Settings.Nick+
        ' -> '+ToNick+' : '+Msg);
  except
    Client.Active:=false;
  end;
end;

procedure TMainForm.SendSystemMessage(Head: String; Msg: string);
begin
  try
    if not(Client.Active) then
      Client.Active:=true;
    Client.Host:=MainChat.IP;
    Client.BroadcastEnabled:=true;
    //Client.Broadcast(Head+Settings.Nick+END_NAME+Msg+' ', Client.Port);
    Client.Send(Head+Settings.Nick+END_NAME+Msg+' ', IndyTextEncoding_UTF8, IndyTextEncoding_UTF8);
  except
    Client.Active:=false;
  end;
end;

function TMainForm.GetUserIndex(UserNick: String; UserIP: String): Integer;
var i, c: Integer;
begin
  Result:=GetUserIndexByIP(UserIP);
  Exit;

  //-----------------------------------------
  Result:=-1;
  with Users do
  begin
    c:=Count;
    if c=0 then Exit;
    for i:=0 to c-1 do
      if Items.Strings[i] = UserNick then
        if Assigned(Items.Objects[i]) then
            if TChatItem(Items.Objects[i]).IP = UserIP then
              begin
                Result:=i;
                Break;
              end;
  end;
end;

function TMainForm.GetUserIndexByIP(UserIP: String): Integer;
var i, c: Integer;
begin
  Result:=-1;
  with Users do
  begin
    c:=Count;
    if c=0 then Exit;
    i:=0;
    while i<c do
    begin
        if Assigned(Items.Objects[i]) then
        begin
            if TChatItem(Items.Objects[i]).IP = UserIP then
              begin
                Result:=i;
                Exit;
              end;
        end;
        inc(i);
    end;
  end;
end;

function TMainForm.AddUserToList(UserNick: String; UserIP: String): Integer;
var i: Integer;
begin
  Result:=-1;

  if UserIP=LocalIP then Exit;
  with Users.Items do
  begin
   i:=Add(UserNick);
   //i:=IndexOf(UserNick);
   Objects[i]:=TChatItem.Create;
   TChatItem(Objects[i]).IP:=UserIP;
   TChatItem(Objects[i]).ListIndex:=i;
   TChatItem(Objects[i]).Nick:=UserNick;
  end;
  Result:=i;
end;

function TMainForm.GetOrAddChatItem(UserNick: String; UserIP: String): TChatItem;
var chatItem: TChatItem;
  i: Integer;
begin
  Result:=nil;
  if UserIP=LocalIP then Exit;
  i:=GetUserIndex(UserNick, UserIP);
  if (i>=0) then
  begin
    Result:=TChatItem(Users.Items.Objects[i]);
  end
  else
  begin
     i:=Users.Items.Add(UserNick);
     chatItem:=TChatItem.Create;
     chatItem.IP:=UserIP;
     chatItem.ListIndex:=i;
     chatItem.Nick:=UserNick;

     Users.Items.Objects[i]:=chatItem;
     Result:=chatItem;
  end;
end;


procedure TMainForm.FormActivate(Sender: TObject);
begin
  ReallyClose:=false;
  if NeedAttension and Assigned(UnreadMessagesForm) then
  begin
     //if IsWindow(UnreadMessagesForm.Handle);
     //if (UnreadMessagesForm.Handle <> INVALID_HANDLE_VALUE) then
     //     SendMessage(UnreadMessagesForm.Handle, CM_RELEASE, 0, 0);
     //UnreadMessagesForm:=nil;
     FreeAndNil(UnreadMessagesForm);
  end;
  NeedAttension:=false;
  if Activated then Exit;

    ApplySettings(Settings);
    try
     Server.Bindings.Add;
     Server.Bindings.Items[0].IP:='0.0.0.0';
     Server.Bindings.Items[0].Port:=Server.DefaultPort;
     Server.Active:=true;
     Client.Host:=MainChat.IP;
     Client.Active:=true;
     Caption:=Settings.Nick;
    except
      ShowMessage(SPortBusy);
      StartError:=true;
      ReallyClose:=true;
      Close;
      Exit;
    end;
  Activated:=true;
  AssignFile(FLog, LOGFILE);
  if Settings.UseLog then OpenLogFile;
  MainChat.Sheet.Msg.SetFocus;
  SendSystemMessage(HEAD_ONLINE, MSGONLINE);
  if (Settings.StartHidden) then
  begin
    ShowInTaskBar:=stNever;
    Hide;
  end;
  WriteSettings(Settings);
end;

procedure TMainForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if (not(ReallyClose)) then
  begin
    ReallyClose:=false;
    CloseAction:=caHide;
    ShowInTaskBar:=stNever;
    Exit;
  end;
  if MessageDlg('Выход', 'Выйти из чата?', mtConfirmation, mbYesNo, 0, mbNo) <> mrYes then
  begin
   CloseAction:=caNone;
   Exit;
  end;

  OnlineTimer.Enabled:=false;
  //if Assigned(UnreadMessagesForm) then
  //begin
  //   //SendMessage(UnreadMessagesForm.Handle, CM_RELEASE, 0, 0);
  //   FreeAndNil(UnreadMessagesForm);
  //end;
  Exiting:=true;
  FreeAndNil(Pict);
  if not(StartError) then
    begin
      SendSystemMessage(HEAD_OFFLINE, MSGOFFLINE);
      FillCurrentSettings(Settings);
      WriteSettings(Settings);
    end;
  FreeUsersObjects;
  MainChat.Free;
  Server.Active:=false;
  Client.Active:=false;
  {$IfDef Linux}
 //  if dsp<>nil then XCloseDisplay(dsp);
  {$EndIf}
  if FileOpened(FLog) then
    CloseFile(FLog);
end;

procedure TMainForm.FormCreate(Sender: TObject);
//var Mode: Word;
begin
  ReallyClose:=false;
  Activated:=false;
  Exiting:=false;
  StartError:=false;
  NeedAttension:=false;
  IconSwitch:=true;
  Pict:=TPicture.Create;
    Settings:=ReadSettings;
    MainChat:=TChatItem.Create;
    MainChat.Nick:=Settings.Nick;
    if (Settings.UseCustomIP and IsIPAddress(Settings.CustomIP)) then
    begin
      LocalIP:=Settings.CustomIP;
    end
    else
    begin
      LocalIP:=GetLocalIP(Settings.UseLastIP);
    end;
    MainChat.TimeOut:=ITEMTIMEOUT;
    MainChat.IP:=GetNetwork(LocalIP);
    MainChat.Sheet:=TNAChatTabSheet.Create(ChatPages);
    with MainChat.Sheet do
    begin
     Parent:=ChatPages;
     ChatItem:=MainChat;
     Caption:=SMainCaption;
     OnSendClick:=@SendChatMessage;
     OnMouseMove:=Self.OnMouseMove;
     Msg.OnMouseMove:=Self.OnMouseMove;
     TextList.OnMouseMove:=Self.OnMouseMove;
     bClose.Visible:=false;
    end;
    Left:=Settings.Left;
    Top:=Settings.Top;
//    ApplySettings(Settings);
end;

procedure TMainForm.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  NeedAttension:=false;
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
   if not(Settings.Maximized) then
     begin
      OldSize.Left:=Settings.Left;
      OldSize.Top:=Settings.Top;
      OldSize.Right:=Settings.Left+Settings.Width;
      OldSize.Bottom:=Settings.Top+Settings.Height;
     end;
   if not(Activated) then Exit;

   Settings.Left:=Left;
   Settings.Top:=Top;
   Settings.Width:=Width;
   Settings.Height:=Height;
end;

procedure TMainForm.FormWindowStateChange(Sender: TObject);
begin
  case WindowState of
    wsMinimized: begin
       ShowInTaskBar:=stNever;
       WindowState:=wsNormal;
       Hide;
     end;
    wsNormal: if Settings.Maximized then
      begin
       Settings.Left:=OldSize.Left;
       Settings.Top:=OldSize.Top;
       Settings.Width:=OldSize.Right-OldSize.Left;
       Settings.Height:=OldSize.Bottom-OldSize.Top;
      end;
  end;
  if WindowState<>wsMinimized then
   Settings.Maximized:=(WindowState=wsMaximized);
end;

procedure TMainForm.miClearLogClick(Sender: TObject);
begin
  if MessageDlg(SClearLogCaption, SClearLog, mtConfirmation, mbYesNo, 0) = mrYes then
    if FileOpened(FLog) then
    begin
     CloseFile(FLog);
     DeleteFile(LOGFILE);
     if Settings.UseLog then OpenLogFile;
    end;
end;

procedure TMainForm.miLogViewClick(Sender: TObject);
var s: String;
begin
  if FileExists(LOGFILE) then
  with LogForm do
  begin
    LogView.Lines.Clear;
    if FileOpened(FLog) then
     begin
      CloseFile(FLog);
     end;
      Reset(FLog);
      while not(EOF(FLog)) do
      begin
       Readln(FLog, s);
       LogView.Lines.Add(s);
      end;
      CloseFile(FLog);
      Append(Flog);
    ShowModal;
  end;
end;

procedure TMainForm.miUsersListClearClick(Sender: TObject);
begin
    Server.Active:=false;
    FreeUsersObjects;
    Users.Items.Clear;
    Server.Active:=true;
end;

procedure TMainForm.TestTimerTimer(Sender: TObject);
begin
  SendChatMessage(MainChat.Sheet, 'Message: ' + DateTimeToStr(Now));
end;

procedure TMainForm.tmiExitClick(Sender: TObject);
begin
  ReallyClose:=true;
  Close;
end;

procedure TMainForm.tmiShowClick(Sender: TObject);
begin
  TrayIconClick(Sender);
end;

procedure TMainForm.miCloseTabClick(Sender: TObject);
begin
  if ChatPages.ActivePage<>MainChat.Sheet then
    CloseChatSheet(TNAChatTabSheet(ChatPages.ActivePage));
end;

initialization
{$I icons.lrs}

end.

