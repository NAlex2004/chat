unit ChatTabSheet;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls, ComCtrls, NATextList, Controls, Graphics,
  LMessages, LCLIntf, LCLType, ipaddr, ChatSettingsType, ExtCtrls;

const
        LM_MYMSG = LM_USER+1;
        LEADSPACE = '   ';
        //ITEMTIMEOUT = 5;
        MAXTEXTLINES = 1000;
type

 TSendChatEvent = procedure(Sender: TObject; Msg: string) of object;

 TChatItem = class;

{ TNAChatTabSheet }

 TNAChatTabSheet = class(TTabSheet)
       private
         FCreated: Boolean;
         FClearClick: TNotifyEvent;
         FCloseClick: TNotifyEvent;
         FFromColor: TColor;
         FFromFontStyles: TFontStyles;
         FFromMsgColor: TColor;
         FFromMsgFontStyles: TFontStyles;
         FOutColor: TColor;
         FOutFontStyles: TFontStyles;
         FOutMsgColor: TColor;
         FOutMsgFontStyles: TFontStyles;
         FSendClick: TSendChatEvent;
         procedure DoCloseClick(Sender: TObject);
         procedure DoSendClick(Sender: TObject);
         procedure DoClearClick(Sender: TObject);
         procedure HandleRelease(var Msg: TLMessage); message CM_RELEASE;
         procedure HandleMyMsg(var Msg: TLMessage); message LM_MYMSG;
         procedure ScrollTextList;
         procedure MsgKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
       protected
         function GetMsgHead: String;
         procedure UpdateChildrenSize;
         procedure DoShow; override;
       public
         ChatItem: TChatItem;
         TextList: TNATextList;
         Msg: TMemo;
         bSend: TButton;
         bClear: TButton;
         bClose: TButton;
         procedure SetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
         constructor Create(TheOwner: TComponent); override;
         destructor Destroy; override;
         property OnCloseClick: TNotifyEvent read FCloseClick write FCloseClick;
         property OnClearClick: TNotifyEvent read FClearClick write FClearClick;
         property OnSendClick: TSendChatEvent read FSendClick write FSendClick;
         procedure AddIncommingMessage(From: String; Message: String);
         procedure AddStatusMessage(From: String; Message: String);
  //       procedure AddOutgoingMessage(From: String; Message: String);
         property FromColor: TColor read FFromColor write FFromColor;
         property FromMsgColor: TColor read FFromMsgColor write FFromMsgColor;
         property FromFontStyles: TFontStyles read FFromFontStyles write FFromFontStyles;
         property FromMsgFontStyles: TFontStyles read FFromMsgFontStyles write FFromMsgFontStyles;
         property OutColor: TColor read FOutColor write FOutColor;
         property OutMsgColor: TColor read FOutMsgColor write FOutMsgColor;
         property OutFontStyles: TFontStyles read FOutFontStyles write FOutFontStyles;
         property OutMsgFontStyles: TFontStyles read FOutMsgFontStyles write FOutMsgFontStyles;
         procedure ClearOldLines;
       published

     end;

 TChatItem = class
       Nick: String;
       IP: String;
       ListIndex: Integer;
       Sheet: TNAChatTabSheet;
       TimeOut: Integer;
       constructor Create;
       destructor Destroy; override;
     end;

resourcestring
      SSend = 'Послать';
      SClear = 'Очистить';
      SClose = 'Закрыть';

const MEMOHEIGHT=100;
      MINTIMEOUT = 7;
var ITEMTIMEOUT: Integer=MINTIMEOUT;
implementation

{ TChatItem }

constructor TChatItem.Create;
begin
  Nick:=GetUserName;//'JOHN DOE';
  IP:='127.0.0.1';
  ListIndex:=-1;
  Sheet:=nil;
  TimeOut:=ITEMTIMEOUT;
end;

destructor TChatItem.Destroy;
begin
  if Assigned(Sheet) then
    Sheet.Free;
  inherited Destroy;
end;

{ TNAChatTabSheet }

procedure TNAChatTabSheet.DoCloseClick(Sender: TObject);
begin
  if Assigned(FCloseClick) then
    FCloseClick(Self);
  PostMessage(Handle, CM_RELEASE, 0, 0);
end;

procedure TNAChatTabSheet.DoSendClick(Sender: TObject);
var i: Integer; Nick: String;
begin
  if Msg.Lines.Count<=0 then Exit;
  if Assigned(FSendClick) then
    FSendClick(Self, Msg.Lines.Text);
{  if Assigned(ChatItem) then
    Nick:=ChatItem.Nick
    else Nick:=''; }
  Nick:=Settings.Nick;
  TextList.Lines.AddStyled(GetMsgHead+Nick+':', FOutColor, FOutFontStyles);
  for i:=0 to Msg.Lines.Count-1 do
   TextList.Lines.AddStyled(LEADSPACE+Msg.Lines.Strings[i], FOutMsgColor, FOutMsgFontStyles);
  Msg.Lines.Clear;
end;

procedure TNAChatTabSheet.DoClearClick(Sender: TObject);
begin
  if Assigned(FClearClick) then
    FClearClick(Sender);
  Msg.Lines.Clear;
end;

procedure TNAChatTabSheet.HandleRelease(var Msg: TLMessage);
begin
  Free;
end;

procedure TNAChatTabSheet.HandleMyMsg(var Msg: TLMessage);
var st:string;
begin
  st:=PChar(msg.wParam);
  TextList.Lines.AddStyled(GetMsgHead+st, clGray, []);
  ClearOldLines;
  //ScrollTextList;
end;

procedure TNAChatTabSheet.ScrollTextList;
var bmp: TBitmap; th: Integer;
begin
  Exit;

  bmp:=TBitmap.Create;
  bmp.Canvas.Font:=TextList.Canvas.Font;
  th:=bmp.Canvas.TextHeight('Q');
  bmp.Free;
//  i:=VertScrollBar.Range-ClientHeight-VertScrollBar.Position;
//VertScrollBar.Position+ClientHeight<VertScrollBar.Range

  TextList.VertScrollBar.Range:=TextList.Lines.Count*th;
  {$IfDef UNIX}
  TextList.VertScrollBar.Position:=TextList.VertScrollBar.Range-TextList.ClientHeight;
  {$Else}
  TextList.VertScrollBar.Position:=TextList.VertScrollBar.Range;
  {$Endif}
end;

procedure TNAChatTabSheet.MsgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssShift in Shift) or (ssCtrl in Shift) then Exit;
  if Key=VK_RETURN then
    begin
     Key:=0;
     DoSendClick(Sender);
    end;
end;

function TNAChatTabSheet.GetMsgHead: String;
begin
  Result:='['+TimeToStr(Time)+'] ';
end;


procedure TNAChatTabSheet.UpdateChildrenSize;
var mHeight, nTop, maxWidth: Integer;
begin
  mHeight:=Height div 4;
  if mHeight>MEMOHEIGHT then
    mHeight:=MEMOHEIGHT;
  if mHeight<bSend.Height*3 then
    mHeight:=bSend.Height*3;
  TextList.SetBounds(0, 0, Width, Height-mHeight);
  nTop:=TextList.Height+1;
  maxWidth:=bSend.Width;
  if bClose.Width>maxWidth then
    maxWidth:=bClose.Width;
  if bClear.Width>maxWidth then
    maxWidth:=bClear.Width;
  bClose.AutoSize:=false;
  bClear.AutoSize:=false;
  bSend.AutoSize:=false;
  with bSend do
  begin
   Left:=Self.Width-maxWidth;
   Width:=maxWidth;
   Top:=nTop;
  end;
  nTop:=nTop+bSend.Height;//+1;
  with bClear do
  begin
   Left:=Self.Width-maxWidth;
   Width:=maxWidth;
   Top:=nTop;
  end;
  nTop:=nTop+bClear.Height;//+1;
  with bClose do
  begin
   Left:=Self.Width-maxWidth;
   Width:=maxWidth;
   Top:=nTop;
  end;
  Msg.SetBounds(0, Height-mHeight+1, Width-maxWidth-1, mHeight-1);
end;

procedure TNAChatTabSheet.DoShow;
begin
  inherited DoShow;
  UpdateChildrenSize;
end;

procedure TNAChatTabSheet.SetBounds(ALeft, ATop, AWidth, AHeight: integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if FCreated then
    begin
     UpdateChildrenSize;
    // FCreated:=false;
    end;
end;

constructor TNAChatTabSheet.Create(TheOwner: TComponent);
begin
  FCreated:=false;
  inherited Create(TheOwner);
  FFromColor:=FRMCOLOR;
  FFromFontStyles:=[fsBold];
  FFromMsgColor:=FRMMSGCOLOR;
  FFromMsgFontStyles:=[];
  FOutColor:=TOCOLOR;
  FOutFontStyles:=[fsBold];
  FOutMsgColor:=TOMSGCOLOR;
  FOutMsgFontStyles:=[];
  TextList:=TNATextList.Create(Self);
  with TextList do
  begin
 //   Anchors:=[akLeft, akRight, akTop, akBottom];
  end;
  bSend:=TButton.Create(Self);
  with bSend do
  begin
    AutoSize:=true;
    Caption:=SSend;
 //   Anchors:=[akRight, akBottom];
  end;

  bClear:=TButton.Create(Self);
  with bClear do
  begin
    AutoSize:=true;
    Caption:=SClear;
//    Anchors:=[akRight, akBottom];
  end;

  bClose:=TButton.Create(Self);
  with bClose do
  begin
    AutoSize:=true;
    Caption:=SClose;
//    Anchors:=[akRight, akBottom];
  end;

  Msg:=TMemo.Create(Self);
  Msg.Anchors:=[akLeft, akRight, akTop, akBottom];
  Msg.AnchorSide[akTop].Control:=TextList;
  Msg.AnchorSide[akTop].Side:=asrBottom;
  Msg.ScrollBars:=ssAutoVertical;
  Msg.OnKeyDown:=@MsgKeyDown;
  bSend.Parent:=Self;
  bClear.Parent:=Self;
  bClose.Parent:=Self;
  Msg.Parent:=Self;
  TextList.Parent:=Self;
  bClose.OnClick:=@DoCloseClick;
  bSend.OnClick:=@DoSendClick;
  bClear.OnClick:=@DoClearClick;
  FCreated:=true;
end;

destructor TNAChatTabSheet.Destroy;
begin
  inherited Destroy;
end;

procedure TNAChatTabSheet.AddIncommingMessage(From: String; Message: String);
var i, l: Integer; s, head: String;
begin

  head:=GetMsgHead + (From) + ':';
  TextList.Lines.AddStyled(head, FromColor, FromFontStyles);

  l:=Length(Message);
  i:=1;
  repeat
    s:='';
    while (i<=l) and not(Message[i] in [#10,#13]) do
     begin
      s:=s+Message[i];
      inc(i);
     end;
    while (i<=l) and (Message[i] in [#10,#13]) do
     inc(i);
    TextList.Lines.AddStyled(LEADSPACE+s, FromMsgColor, FromMsgFontStyles);
  until i>l;
//  ScrollTextList;
  ClearOldLines;
end;

procedure TNAChatTabSheet.AddStatusMessage(From: String; Message: String);
begin
  TextList.Lines.AddStyled(GetMsgHead+(From)+' '+(Message), STATUSMSGCOLOR, []);
  ClearOldLines;
end;

procedure TNAChatTabSheet.ClearOldLines;
begin
  with TextList do
    if Lines.Count>MAXTEXTLINES then
    begin
      Lines.DisableOnChange;
      while Lines.Count>MAXTEXTLINES do
        Lines.Delete(0);
      Lines.EnableOnChange;
      Invalidate;
      CalculateRange;
      VertScrollBar.Position:=VertScrollBar.Range-ClientHeight;
    end;
end;

{
procedure TNAChatTabSheet.AddOutgoingMessage(From: String; Message: String);
begin
  TextList.Lines.AddStyled(From, Color, FromFontStyles);
  TextList.Lines.AddStyled(Message, MessageColor, MessageFontStyles);
end;
}
end.

