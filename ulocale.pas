unit uLocale;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

resourcestring
   SMainCaption = 'Основной чат';
   SResetSettings = 'Вернуть изначальные настройки?';
   SResetCaption = 'Сброс настроек.';
   SPortBusy = 'Чат уже запущен, или порт занят.';
   SUser = 'Пользователь';
   SUserOnline = 'вошел в чат.';
   SUserOffline = 'вышел из чата.';
   SChatHint = 'Чат: ';
   SUserRenamed = 'имя изменено на ';
   SLogError = 'Не могу создать/открыть файл истории.'+LineEnding+'Ведение истории будет отключено.';
   SAll = 'Всем';
   SClearLogCaption = 'Очистка истории.';
   SClearLog = 'Очистить файл истории сообщений?';
implementation

end.

