#!/bin/sh
#dynamic
gcc -lX11 -fPIC -c xtime.c
gcc -shared -fPIC -o libxtime.so xtime.o
#static
gcc -lX11 -c -o libxtime-static.o xtime.c
ar rcs libxtime-static.a libxtime-static.o
#in project:
#{$linklib libX11.so}
#{$L libxtime-static.a}