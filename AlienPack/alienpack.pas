{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit alienpack;

interface

uses
  PrgStatusBar, LabEdit, Eyes, fillunit, SimpleEyes, RunLabel, NAStringList, 
  NATextList, natextlistlang, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('PrgStatusBar', @PrgStatusBar.Register);
  RegisterUnit('LabEdit', @LabEdit.Register);
  RegisterUnit('Eyes', @Eyes.Register);
  RegisterUnit('SimpleEyes', @SimpleEyes.Register);
  RegisterUnit('RunLabel', @RunLabel.Register);
  RegisterUnit('NATextList', @NATextList.Register);
end;

initialization
  RegisterPackage('alienpack', @Register);
end.
