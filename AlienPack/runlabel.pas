unit RunLabel;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, Controls, ExtCtrls, LResources{, LogUnit};

type TRLDirection=(rdRightToLeft, rdLeftToRight);
     TRLRunMode=(rmOnce, rmRepeat, rmContinuous, rmNone);

type

{ TNARunLabel }

 TNARunLabel=class(TGraphicControl)
      private
        FAutoWidth: boolean;
        FBorder: boolean;
        FBorderColor: TColor;
        FCaption: TCaption;
        FContTimes: integer;
        FDelay: integer;
        FStep: byte;
        FTempDelay:integer;
        FDirection: TRLDirection;
        FEnabled: Boolean;
        FInterval: integer;
        FRunMode: TRLRunMode;
        WRunMode: TRLRunMode;
        WContTimes: integer;
        Fx:integer;  //позиция текста по X
        Bmp:TBitmap; //тут рисуем, потом переносим
        procedure SetAutoWidth(AValue: boolean);
        procedure SetBorder(AValue: boolean);
        procedure SetBorderColor(AValue: TColor);
        procedure SetCaption(AValue: TCaption);
        procedure SetContTimes(AValue: integer);
        procedure SetDirection(AValue: TRLDirection);
        procedure SetInterval(AValue: integer);
        procedure SetRunMode(AValue: TRLRunMode);
        procedure SetStep(AValue: byte);
        function GetComponentNumber: Integer;
      protected
        FTimer:TTimer;
        procedure DoReStart;
        procedure Paint; override;
        procedure RunOnTimer(Sender:TObject); virtual;
        procedure SetEnabled(AValue: Boolean); override;
        procedure Loaded; override; //а это только при загрузке из файла формы, т.е. в рантайме не работает
        procedure FontChanged(Sender: TObject); override;
      public
        constructor Create(AOwner: TComponent); override;
        destructor Destroy; override;
        procedure SetBounds(aLeft, aTop, aWidth, aHeight: integer); override;
        procedure ReStart;
      published
        property Caption:TCaption read FCaption write SetCaption;
        property Direction:TRLDirection read FDirection write SetDirection;
        property RunMode:TRLRunMode read FRunMode write SetRunMode;
        property Enabled:Boolean read FEnabled write SetEnabled;
        property Interval:integer read FInterval write SetInterval;
        property Delay:integer read FDelay write FDelay;
        property AutoWidth:boolean read FAutoWidth write SetAutoWidth;
        property ContinuousTimes:integer read FContTimes write SetContTimes;
        property Border:boolean read FBorder write SetBorder;
        property BorderColor:TColor read FBorderColor write SetBorderColor;
        property Step:byte read FStep write SetStep;
      published
        property OnClick;
        property OnMouseDown;
        property OnDblClick;
        property OnMouseUp;
        property Visible;
        property Color;
        property Font;
     end;

 procedure Register;

implementation
procedure Register;
begin
  {$I runlabel_icon.lrs}
  RegisterComponents('AlienPack',[TNARunLabel]);
end;

{ TNARunLabel }

procedure TNARunLabel.SetCaption(AValue: TCaption);
begin
  if FCaption=AValue then Exit;
  FCaption:=AValue;
  SetBounds(left,top,Width,Height);
end;

procedure TNARunLabel.SetContTimes(AValue: integer);
begin
  if FContTimes=AValue then Exit;
  FContTimes:=AValue;
  WContTimes:=AValue;
end;

procedure TNARunLabel.SetBorder(AValue: boolean);
begin
  if FBorder=AValue then Exit;
  FBorder:=AValue;
  Repaint;
end;

procedure TNARunLabel.SetAutoWidth(AValue: boolean);
begin
  if FAutoWidth=AValue then Exit;
  FAutoWidth:=AValue;
  SetBounds(Left, Top, Width, Height);
end;

procedure TNARunLabel.SetBorderColor(AValue: TColor);
begin
  if FBorderColor=AValue then Exit;
  FBorderColor:=AValue;
  Repaint;
end;

procedure TNARunLabel.SetDirection(AValue: TRLDirection);
begin
  if FDirection=AValue then Exit;
  FDirection:=AValue;
  DoReStart;
end;

procedure TNARunLabel.SetEnabled(AValue: Boolean);
begin
  if FEnabled=AValue then Exit;
  FEnabled:=AValue;
  if not (csLoading in ComponentState) and not (csDesigning in ComponentState)
   then FTimer.Enabled:=FEnabled;
end;

procedure TNARunLabel.Loaded;
begin
  inherited Loaded;
  Bmp.Canvas.Font:=Font;
  Canvas.Font:=Font;
  if AutoWidth then
   Width:=canvas.TextWidth(Caption);
  Bmp.SetSize(Width, Height);
  if FDirection=rdLeftToRight then Fx:=-Canvas.TextWidth(FCaption)-10
   else Fx:=Width+10;
  if FRunMode=rmNone then Fx:=0;
  if not(csDesigning in ComponentState) then
   FTimer.Enabled:=true;
  WRunMode:=FRunMode;
  WContTimes:=FContTimes;
  Paint;
end;

procedure TNARunLabel.FontChanged(Sender: TObject);
begin
  inherited FontChanged(Sender);
  Height:=abs(font.Height);
end;

procedure TNARunLabel.SetInterval(AValue: integer);
begin
  if FInterval=AValue then Exit;
  FInterval:=AValue;
  if FInterval<10 then FInterval:=10;
  if FInterval>10000 then FInterval:=10000;
  FTimer.Interval:=FInterval;
end;

procedure TNARunLabel.SetRunMode(AValue: TRLRunMode);
begin
  if FRunMode=AValue then Exit;
  FRunMode:=AValue;
  WRunMode:=AValue;
  DoReStart;
end;

procedure TNARunLabel.SetStep(AValue: byte);
begin
  if FStep=AValue then Exit;
  if AValue<1 then AValue:=1;
  FStep:=AValue;
end;

function TNARunLabel.GetComponentNumber: Integer;
var i, c: Integer;
begin
  if Assigned(Parent) then
   with Parent do
   begin
    c:=0;
    for i:=0 to ComponentCount-1 do
      if Components[i] is TNARunLabel then
        inc(c);
    Result:=c;
   end
  else
   Result:=ComponentIndex;
end;

procedure TNARunLabel.DoReStart;
begin
  if FDirection=rdLeftToRight then Fx:=-Canvas.TextWidth(FCaption)-10
   else Fx:=Width+10;
  if FRunMode=rmNone then Fx:=0;
  Repaint;
end;

procedure TNARunLabel.Paint;
begin
  if not Visible then
   if not (csDesigning in ComponentState) then Exit;
  inherited Paint;
  Bmp.Canvas.Brush.Color:=Color;
  Bmp.Canvas.Brush.Style:=bsSolid;
  Bmp.Canvas.FillRect(0, 0,Width, Height);
  if Border then
   begin
    Bmp.Canvas.Pen.Color:=BorderColor;
    Bmp.Canvas.Rectangle(0,0,Width,Height);
   end;
  Bmp.Canvas.Brush.Style:=bsClear;
  if not (csDesigning in ComponentState) then
    Bmp.Canvas.TextOut(Fx+1, 1, FCaption)
   else
     Bmp.Canvas.TextOut(1, 1, FCaption);
  canvas.Draw(0,0,bmp);

end;

procedure TNARunLabel.RunOnTimer(Sender:TObject);
var FxNew:integer;
begin
 if (WRunMode=rmNone) or not(Enabled) then Exit;
 if FDirection=rdLeftToRight then FxNew:=Fx+Step
  else FxNew:=Fx-Step;
 case WRunMode of
  rmOnce:  if Fx=0 then
            begin
             WRunMode:=rmNone;
             exit;
            end;

  rmRepeat: begin    //двигаем, потом ждем Delay циклов, потом снова двигаем
             if FTempDelay>0 then
              begin
               dec(FTempDelay);
               if FTempDelay=0 then
                case FDirection of
                 rdRightToLeft: Fx:=Width+10;
                 rdLeftToRight: Fx:=-Canvas.TextWidth(Caption)-10;
                end;
               exit;
              end;
             if Fx=0 then
             begin
              FTempDelay:=Delay;
              exit;
             end;
            end;
  rmContinuous:  case FDirection of
                  rdRightToLeft:  if (Fx+Canvas.TextWidth(Caption))<0 then
                                  begin
                                   Fx:=Width;
                                   if WContTimes<=0 then
                                    exit;
                                   Dec(WContTimes);
                                   if WContTimes<=0 then
                                    WRunMode:=rmOnce;
                                   exit;
                                  end;
                  rdLeftToRight: if Fx>Width then
                                  begin
                                   Fx:=-Canvas.TextWidth(Caption);
                                   if WContTimes<=0 then
                                    exit;
                                   Dec(WContTimes);
                                   if WContTimes<=0 then
                                    WRunMode:=rmOnce;
                                   exit;
                                  end;
                 end;


 end;
 if WRunMode<>rmContinuous then
   case FDirection of
    rdRightToLeft: if FxNew<0 then FxNew:=0;
    rdLeftToRight: if FxNew>0 then FxNew:=0;
   end;

 Fx:=FxNew;
 RePaint;

end;

constructor TNARunLabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Parent:=AOwner as TWinControl;
  Bmp:=TBitmap.Create;
  FCaption:='RunLabel'+IntToStr(GetComponentNumber);
  //inttostr(self.ComponentIndex);
  FDirection:=rdRightToLeft;
  FRunMode:=rmNone;//rmContinuous;
  WRunMode:=FRunMode;
  Visible:=true;
  Font.name:='default';//GetFontData(Parent.Font.Handle).Name;
  Font.Height:=Canvas.TextHeight(caption);//abs(Parent.Font.Height);
  Font.Color:=clBtnText;
  Bmp.Canvas.Font:=Font;
  Color:=clForm;//clBtnFace;
  FDelay:=10;
  FTempDelay:=10;
  FTimer:=TTimer.Create(self);
  FTimer.Interval:=10;
  FInterval:=10;;
  FTimer.Enabled:=false;
  FTimer.OnTimer:=@RunOnTimer;
  Width:=Canvas.TextWidth(Caption)+4;
  FContTimes:=-1;
  WContTimes:=FContTimes;
  FBorder:=false;
  FStep:=1;
  Fx:=0;//Width+5;
  Enabled:=true;
end;

destructor TNARunLabel.Destroy;
begin
 FTimer.Enabled:=false;
 FreeAndNil(FTimer);
 FreeAndNil(Bmp);
 inherited Destroy;
end;

procedure TNARunLabel.SetBounds(aLeft, aTop, aWidth, aHeight: integer);
begin
  aHeight:=abs(Font.Height)+4;
  Bmp.Canvas.Font:=font;
  if AutoWidth then aWidth:=bmp.Canvas.TextWidth(Caption)+4;
  inherited SetBounds(aLeft, aTop, aWidth, aHeight);
  Bmp.SetSize(aWidth, aHeight);
  Repaint;
end;

procedure TNARunLabel.ReStart;
begin
  WRunMode:=FRunMode;
  WContTimes:=FContTimes;
  DoReStart;
end;

end.

