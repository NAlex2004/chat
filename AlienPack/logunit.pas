unit LogUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil;
const DEFMAXSIZE = 10485760; //10Mb
type

{ TLogFile }

 TLogFile=class
   private
     LogFile: TextFile;
     Opened: Boolean;
     LastMsgPos: Longint;
     LastMsg: String;
     LastPos: Longint;
     function GetNameNoExtension(FName: String): String;
   public
     AddDate: Boolean;
     MaxFileSize: DWord;
     //LastChanged: Boolean;
     procedure StartLog;
     procedure WriteLog(msg:string);
     procedure CloseLog;
     procedure ClearLog;
     function ReadLast:string;
     function ReadAll: TStringList;
     procedure StopLog;
     procedure BackUpLog;
     procedure BackUpLog(FileName:string); overload;
     constructor Create(UseDateTime: boolean; MaxSize: DWord = DEFMAXSIZE);//10Mb
     constructor Create(UseDateTime: boolean; FileName:string; MaxSize: DWord = DEFMAXSIZE); overload;
     destructor Destroy; override;
  end;

procedure logWriteLog(msg:string);
function logReadLast:string;
function logReadAll: TStringList;
procedure logBackUpLog;
procedure logClearLog;
procedure FreeDefaultLog;

implementation
var DefLog:TLogFile;

procedure logWriteLog(msg: string);
begin
  DefLog.WriteLog(msg);
end;

function logReadLast: string;
begin
 Result:=DefLog.ReadLast;
end;

function logReadAll: TStringList;
begin
 Result:=DefLog.ReadAll;
end;

procedure logBackUpLog;
begin
 DefLog.BackUpLog;//('BackUp-'+ApplicationName+'-Log'+'.bak');
end;

procedure logClearLog;
begin
  DefLog.ClearLog;
end;

procedure FreeDefaultLog;
begin
  FreeAndNil(DefLog);
end;



 { TLogFile }

 function TLogFile.GetNameNoExtension(FName: String): String;
 var l: Integer;
 begin
   Result:=FName;
   if FName = '' then Exit;
   l:=Length(FName);
   while (FName[l]<>ExtensionSeparator) and (l>1) do Dec(l);
   if l=1 then Exit;
   Result:=Copy(FName, 1, l-1)
 end;

 procedure TLogFile.StartLog;
 begin
  //AssignFile(LogFile,'');
  Rewrite(LogFile);
  Opened:=true;
  LastMsg:='';
  LastMsgPos:=0;
  LastPos:=0;
 end;

 procedure TLogFile.ClearLog;
 begin
  CloseLog;
  StartLog;
 end;

 procedure TLogFile.WriteLog(msg: string);
 var s:string;
 begin
  if FileSize(TTextRec(LogFile).name)>MaxFileSize then
   begin
     WriteLn(LogFile, 'MAXIMUM LOG SIZE REACHED.');
     BackUpLog;
     ClearLog;
     WriteLn(LogFile, 'MAXIMUM LOG SIZE REACHED. Log backed up. New log started.');
   end;
  if not(Opened) then
   StartLog;
  s:='';
  if AddDate then s:=s+DateTimeToStr(now);
  s:=s + ' = ' + msg;
  WriteLn(LogFile, Utf8ToAnsi(s));
  Flush(LogFile);
  LastMsg:=s;
  //PreLastPos:=LastPos;
  LastPos:=LastPos+1;
 end;

 procedure TLogFile.CloseLog;
 begin
  CloseFile(LogFile);
  Opened:=false;
 end;

 function TLogFile.ReadLast: string;
 //returns '' if las message is the same as pre last
 // or no messages yet loged
 begin
   if LastMsgPos<>LastPos then
    begin
      Result:=LastMsg;
      LastMsgPos:=LastPos;
    end
    else
     Result:='';
 end;

 function TLogFile.ReadAll: TStringList;
 var R:TStringList; FName: string;
 begin
  if LastPos=0 then exit;
  R:=TStringList.Create;
  FName:=TTextRec(LogFile).name;
  CloseFile(LogFile);
  R.LoadFromFile(FName);
{  Reset(LogFile);
  while not eof(LogFile) do
  begin
    ReadLn(LogFile, s);
    R.Add(AnsiToUtf8(s));
  end;
  CloseFile(LogFile);}
  Append(LogFile);
  Result:=R;
 end;

 procedure TLogFile.StopLog;
 begin
   CloseLog;
 end;

 procedure TLogFile.BackUpLog;
 var FName, LogName: string; SL:TStringList; i: DWord;
 begin
  CloseFile(LogFile);
  LogName:=TTextRec(LogFile).name;
  FName:=GetNameNoExtension(LogName)+'_log';
  if FileExistsUTF8(FName+'.bak') then
   begin
    i:=1;
    while FileExistsUTF8(FName+'_'+IntToStr(i)+'.bak') do Inc(i);
    FName:=FName+'_'+IntToStr(i)+'.bak';
   end
  else FName:=FName+'.bak';
  SL:=TStringList.Create;
  SL.LoadFromFile(LogName);
  SL.SaveToFile(FName);
  SL.Free;
  Append(LogFile);
  WriteLog('Log file backup complete in '+FName);//LogFile.bak');
 end;

 procedure TLogFile.BackUpLog(FileName: string);
 var FName: string; SL: TStringList;
 begin
  CloseFile(LogFile);
  FName:=TTextRec(LogFile).name;
  SL:=TStringList.Create;
  SL.LoadFromFile(FName);
  SL.SaveToFile(FileName);
  SL.Free;
  Append(LogFile);
  WriteLog('Log file backup complete in '+FileName+'.');
 end;

  constructor TLogFile.Create(UseDateTime: boolean; MaxSize: DWord = DEFMAXSIZE);
 begin
   AddDate:=UseDateTime;
   AssignFile(LogFile,'LogFile.log');
   Opened:=false;
   MaxFileSize:=MaxSize;
 end;

  constructor TLogFile.Create(UseDateTime: boolean; FileName: string; MaxSize: DWord = DEFMAXSIZE);
  begin
   AddDate:=UseDateTime;
   AssignFile(LogFile, FileName);
   Opened:=false;
   MaxFileSize:=MaxSize;
  end;

 destructor TLogFile.Destroy;
 begin
  if Opened then CloseLog;
  inherited Destroy;
 end;

initialization
begin
 DefLog:=TLogFile.Create(true, ApplicationName+'.log');
 DefLog.StartLog;
 DefLog.WriteLog('-------------Default Log started.--------------');
end;

finalization
begin
 if Assigned(DefLog) then
  begin
   DefLog.WriteLog('-------------Default Log finished.--------------');
   FreeAndNil(DefLog);
  end;
end;
end.

