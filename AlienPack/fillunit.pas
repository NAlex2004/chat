unit fillunit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, IntfGraphics, FPimage;

type
   PXYStack=^TXYStack;
   TXYStack=record
                x:integer;
                y:integer;
                prev:PXYStack;
              end;
   procedure PushXY(xy:TPoint);
   function PopXY(var xy:TPoint):boolean; //true if not empty stack
   procedure FloodFillRec(var Bitmap: TBitmap; FillColor: TColor;
  BorderColor: TColor; xx, yy: integer; leftx,topy,rightx,bottomy:integer);
   //нормально так и рекурсией, не сильно тормознуто
   procedure FloodFillLines(var Bitmap: TBitmap; FillColor: TColor;
  BorderColor: TColor; xx, yy: integer; leftx,topy,rightx,bottomy:integer);
    //самая быстрая хрень
   procedure FreeStack;
   procedure FloodFillLinesCanvas(var bitmap:TBitmap; FillColor: TColor;
  BorderColor: TColor; xx, yy: integer; leftx, topy, rightx, bottomy: integer);
    //самая долгая хрень
implementation
var StackPointer,pxy:PXYStack;

procedure FloodFillRec(var Bitmap: TBitmap; FillColor: TColor;
  BorderColor: TColor; xx, yy: integer; leftx,topy,rightx,bottomy:integer);
var LImg:TLazIntfImage; BCol, FCol: TFPColor;
  procedure Fill(var Img:TLazIntfImage; x,y:integer);
  begin
   Img.Colors[x,y]:=FCol;
   if (x-1>=leftX)and(img.Colors[x-1,y]<>BCol)and(img.Colors[x-1,y]<>FCol) then Fill(Img,x-1,y);
   if (x+1<=rightX)and(img.Colors[x+1,y]<>BCol)and(img.Colors[x+1,y]<>FCol) then Fill(Img,x+1,y);
   if (y-1>=topY)and(img.Colors[x,y-1]<>BCol)and(img.Colors[x,y-1]<>FCol) then Fill(Img,x,y-1);
   if (y+1<=bottomY)and(img.Colors[x,y+1]<>BCol)and(img.Colors[x,y+1]<>FCol) then Fill(Img,x,y+1);
  end;


begin
  if topY>=bottomY then exit;
  if leftX>=rightX then exit;
  if (rightX>bitmap.width) or (bottomY>bitmap.Height) or (leftX<0) or (topY<0) then exit;

  LImg:=Bitmap.CreateIntfImage;
  BCol:=TColorToFPColor(BorderColor);
  FCol:=TColorToFPColor(FillColor);
  Fill(LImg,xx,yy);
  Bitmap.LoadFromIntfImage(LImg);
  FreeAndNil(LImg);
end;

procedure FloodFillLines(var Bitmap: TBitmap; FillColor: TColor;
  BorderColor: TColor; xx, yy: integer; leftx, topy, rightx, bottomy: integer);
var LImg:TLazIntfImage; BCol, FCol: TFPColor;
    ActivePoint:TPoint; XLeft, XRight, XTemp:integer;
    Flag:boolean;
begin
  if topY>=bottomY then exit;
  if leftX>=rightX then exit;
  if (rightX>bitmap.width) or (bottomY>bitmap.Height) or (leftX<0) or (topY<0) then exit;

  LImg:=Bitmap.CreateIntfImage;
  BCol:=TColorToFPColor(BorderColor);
  FCol:=TColorToFPColor(FillColor);

  ActivePoint.x:=xx;
  ActivePoint.y:=yy;
  PushXY(ActivePoint);
  while PopXY(ActivePoint) do
  begin
    LImg.Colors[ActivePoint.x, ActivePoint.y]:=FCol;
    XTemp:=ActivePoint.x;
    inc(ActivePoint.x);
    while (LImg.Colors[ActivePoint.x, ActivePoint.y]<>BCol) and (ActivePoint.x<rightx) do
    begin //заливка вправо
      LImg.Colors[ActivePoint.x, ActivePoint.y]:=FCol;
      inc(ActivePoint.x);
    end;
    XRight:=ActivePoint.x-1;
    ActivePoint.x:=XTemp-1;
    while (LImg.Colors[ActivePoint.x, ActivePoint.y]<>BCol) and (ActivePoint.x>leftx) do
    begin //заливка влево
      LImg.Colors[ActivePoint.x, ActivePoint.y]:=FCol;
      dec(ActivePoint.x);
    end;
    XLeft:=ActivePoint.x+1;
   //--------посмотрим верхнюю строчку--------------
   // Y:=ActivePoint.y;
    ActivePoint.x:=XLeft;
    dec(ActivePoint.y);
    if ActivePoint.y>topY then
    while ActivePoint.x<=XRight do
    begin
     flag:=false;
     while (LImg.Colors[ActivePoint.x, ActivePoint.y]<>BCol)
           and (LImg.Colors[ActivePoint.x, ActivePoint.y]<>FCol)
           and (ActivePoint.x<XRight) do
        begin
            if not(flag) then flag:=true;
            inc(ActivePoint.x);
        end;
     if flag then
      if (ActivePoint.x=XRight) and (LImg.Colors[ActivePoint.x, ActivePoint.y]<>BCol)
        and (LImg.Colors[ActivePoint.x, ActivePoint.y]<>FCol)
        then PushXY(ActivePoint)   //крайний правый пиксел интервала в стек.
        else                       //если несколько отдельных точек границы найдется,
          begin                   //то в стеке будут все точки перед ними
            dec(ActivePoint.x);
            PushXY(ActivePoint);
            inc(ActivePoint.x);
          end;
     flag:=false;
     XTemp:=ActivePoint.x;
     while ((LImg.Colors[ActivePoint.x, ActivePoint.y]=BCol)   //пролистываем границу или заполненные
           or (LImg.Colors[ActivePoint.x, ActivePoint.y]=FCol))
           and (ActivePoint.x<XRight) do
        inc(ActivePoint.x);
      if ActivePoint.x=XTemp then inc(ActivePoint.x);

    end;
   //-------------------------------------

   //--------посмотрим нижнюю строчку--------------
    //ActivePoint.y:=Y+1;
    inc(ActivePoint.y,2);
    ActivePoint.x:=XLeft;
    if ActivePoint.y<bottomY then
    while ActivePoint.x<=XRight do
    begin
     flag:=false;
     while (LImg.Colors[ActivePoint.x, ActivePoint.y]<>BCol)
           and (LImg.Colors[ActivePoint.x, ActivePoint.y]<>FCol)
           and (ActivePoint.x<XRight) do
        begin
            if not(flag) then flag:=true;
            inc(ActivePoint.x);
        end;
     if flag then
      if (ActivePoint.x=XRight) and (LImg.Colors[ActivePoint.x, ActivePoint.y]<>BCol)
        and (LImg.Colors[ActivePoint.x, ActivePoint.y]<>FCol)
        then PushXY(ActivePoint)   //крайний правый пиксел интервала в стек.
        else                       //если несколько отдельных точек границы найдется,
          begin                   //то в стеке будут все точки перед ними
            dec(ActivePoint.x);
            PushXY(ActivePoint);
            inc(ActivePoint.x);
          end;
     flag:=false;
     XTemp:=ActivePoint.x;
     while ((LImg.Colors[ActivePoint.x, ActivePoint.y]=BCol)   //пролистываем границу или заполненные
           or (LImg.Colors[ActivePoint.x, ActivePoint.y]=FCol))
           and (ActivePoint.x<XRight) do
        inc(ActivePoint.x);
      if ActivePoint.x=XTemp then inc(ActivePoint.x);

    end;
   //-------------------------------------

  end;

  Bitmap.LoadFromIntfImage(LImg);
  FreeAndNil(LImg);
  FreeStack;
end;

procedure FloodFillLinesCanvas(var bitmap: TBitmap; FillColor: TColor;
  BorderColor: TColor; xx, yy: integer; leftx, topy, rightx, bottomy: integer);
var //LImg:TLazIntfImage; BCol, FCol: TFPColor;
    ActivePoint:TPoint; XLeft, XRight, XTemp:integer;
    Flag:boolean;
begin
  //самая долго работающая заливка, т.к. через bitmap.canvas.pixels
  if topY>=bottomY then exit;
  if leftX>=rightX then exit;
  if (rightX>bitmap.Canvas.width) or (bottomY>bitmap.Canvas.Height) or (leftX<0) or (topY<0) then exit;

  ActivePoint.x:=xx;
  ActivePoint.y:=yy;
  PushXY(ActivePoint);
  while PopXY(ActivePoint) do
  begin
    bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]:=FillColor;
    XTemp:=ActivePoint.x;
    inc(ActivePoint.x);
    while (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>BorderColor) and (ActivePoint.x<rightx) do
    begin //заливка вправо
      bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]:=FillColor;
      inc(ActivePoint.x);
    end;
    XRight:=ActivePoint.x-1;
    ActivePoint.x:=XTemp-1;
    while (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>BorderColor) and (ActivePoint.x>leftx) do
    begin //заливка влево
      bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]:=FillColor;
      dec(ActivePoint.x);
    end;
    XLeft:=ActivePoint.x+1;
   //--------посмотрим верхнюю строчку--------------
   // Y:=ActivePoint.y;
    ActivePoint.x:=XLeft;
    dec(ActivePoint.y);
    if ActivePoint.y>topY then
    while ActivePoint.x<=XRight do
    begin
     flag:=false;
     while (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>BorderColor)
           and (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>FillColor)
           and (ActivePoint.x<XRight) do
        begin
            if not(flag) then flag:=true;
            inc(ActivePoint.x);
        end;
     if flag then
      if (ActivePoint.x=XRight) and (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>BorderColor)
        and (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>FillColor)
        then PushXY(ActivePoint)   //крайний правый пиксел интервала в стек.
        else                       //если несколько отдельных точек границы найдется,
          begin                   //то в стеке будут все точки перед ними
            dec(ActivePoint.x);
            PushXY(ActivePoint);
            inc(ActivePoint.x);
          end;
     flag:=false;
     XTemp:=ActivePoint.x;
     while ((bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]=BorderColor)   //пролистываем границу или заполненные
           or (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]=FillColor))
           and (ActivePoint.x<XRight) do
        inc(ActivePoint.x);
      if ActivePoint.x=XTemp then inc(ActivePoint.x);

    end;
   //-------------------------------------

   //--------посмотрим нижнюю строчку--------------
    //ActivePoint.y:=Y+1;
    inc(ActivePoint.y,2);
    ActivePoint.x:=XLeft;
    if ActivePoint.y<bottomY then
    while ActivePoint.x<=XRight do
    begin
     flag:=false;
     while (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>BorderColor)
           and (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>FillColor)
           and (ActivePoint.x<XRight) do
        begin
            if not(flag) then flag:=true;
            inc(ActivePoint.x);
        end;
     if flag then
      if (ActivePoint.x=XRight) and (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>BorderColor)
        and (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]<>FillColor)
        then PushXY(ActivePoint)   //крайний правый пиксел интервала в стек.
        else                       //если несколько отдельных точек границы найдется,
          begin                   //то в стеке будут все точки перед ними
            dec(ActivePoint.x);
            PushXY(ActivePoint);
            inc(ActivePoint.x);
          end;
     flag:=false;
     XTemp:=ActivePoint.x;
     while ((bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]=BorderColor)   //пролистываем границу или заполненные
           or (bitmap.canvas.pixels[ActivePoint.x, ActivePoint.y]=FillColor))
           and (ActivePoint.x<XRight) do
        inc(ActivePoint.x);
      if ActivePoint.x=XTemp then inc(ActivePoint.x);

    end;
   //-------------------------------------

  end;
  FreeStack;
end;

procedure FreeStack;
begin
 if StackPointer<>nil then
 while StackPointer<>nil do
 begin
    pxy:=StackPointer;
    StackPointer:=pxy^.prev;
    Dispose(pxy);
 end;
 pxy:=nil;
end;


procedure PushXY(xy: TPoint);
begin
 New(pxy);
 pxy^.x:=xy.x;
 pxy^.y:=xy.y;
 if StackPointer<>nil then
   pxy^.prev:=StackPointer
   else pxy^.prev:=nil;
 StackPointer:=pxy;
end;

function PopXY(var xy: TPoint):boolean;
begin
 result:=false;
 if StackPointer=nil then exit;
 result:=true;
 xy.x:=StackPointer^.x;
 xy.y:=StackPointer^.y;
 pxy:=StackPointer;
 StackPointer:=StackPointer^.prev;
 Dispose(pxy);
 pxy:=nil;
end;

initialization
begin
 StackPointer:=nil;
 pxy:=nil;
end;

finalization
begin
 FreeStack;
end;

end.

