{$mode delphi}

unit ipget;

interface

uses
SysUtils, unixtype, sockets, baseunix, unix, classes, process;

function GetIpList: string;
function GetIpAddrList: String;

const 
  MAX_ADJUSTMENT = 10;
  IPPROTO_IP     = 0;
  IF_NAMESIZE    = 16;
  SIOCGIFCONF    = $8912;
  SIOCGIFHWADDR  = $8927;

type
  {$packrecords c}
  tifr_ifrn = record
    case integer of
      0 : (ifrn_name: array [0..IF_NAMESIZE-1] of char);
  end;

  TIFrec = record
    ifr_ifrn : tifr_ifrn;
    case integer of
      0 : (ifru_addr      : TSockAddr);
      1 : (ifru_dstaddr   : TSockAddr);
      2 : (ifru_broadaddr : TSockAddr);
      3 : (ifru_netmask   : TSockAddr);
      4 : (ifru_hwaddr    : TSockAddr);
      5 : (ifru_flags     : word); 
      6 : (ifru_ivalue    : longint);
      7 : (ifru_mtu       : longint);
//      8 : (ifru_map       : tifmap);
      9 : (ifru_slave     : Array[0..IF_NAMESIZE-1] of char);
      10 : (ifru_newname  : Array[0..IF_NAMESIZE-1] of char);
      11 : (ifru_data     : pointer);
  end; 

  TIFConf = record
    ifc_len : longint;
    case integer of
      0 : (ifcu_buf : pointer);
      1 : (ifcu_req : ^tifrec);
  end;


implementation

function GetIpAddrList: String;
var
  AProcess: TProcess;
  s: string;
  sl: TStringList;
  i, n: integer;
  noAddrWord: boolean;
begin
  Result:='';
  sl:=TStringList.Create();
  {$IFDEF WINDOWS}
  AProcess:=TProcess.Create(nil);
  AProcess.CommandLine := 'ipconfig.exe';
  AProcess.Options := AProcess.Options + [poUsePipes, poNoConsole];
  try
    AProcess.Execute();
    Sleep(500); // poWaitOnExit don't work as expected
    sl.LoadFromStream(AProcess.Output);
  finally
    AProcess.Free();
  end;
  for i:=0 to sl.Count-1 do
  begin
    if (Pos('IPv4', sl[i])=0) and (Pos('IP-', sl[i])=0) and (Pos('IP Address', sl[i])=0) then Continue;
    s:=sl[i];
    s:=Trim(Copy(s, Pos(':', s)+1, 999));
    if Pos(':', s)>0 then Continue; // IPv6
    if Result.Length > 0 then
      Result:=Result + ',';
    Result:=Result+s;
  end;
  {$ENDIF}
  {$IFDEF UNIX}
  AProcess:=TProcess.Create(nil);
  AProcess.CommandLine := '/sbin/ifconfig';
  AProcess.Options := AProcess.Options + [poUsePipes, poWaitOnExit];
  try
    AProcess.Execute();
    sl.LoadFromStream(AProcess.Output);
  finally
    AProcess.Free();
  end;

  for i:=0 to sl.Count-1 do
  begin
    noAddrWord:=false;
    n:=Pos('inet addr:', sl[i]);
    if n=0 then
    begin
      n:=Pos('inet ', sl[i]);
      noAddrWord:=true;
    end;
    if n=0 then Continue;
    s:=sl[i];
    if noAddrWord then // in 18.10, may be earlie, ifconfig shows: inet 127.0.0.1, not inet addr:
    begin
      s:=Copy(s, n+Length('inet '), 999);
    end
    else
    begin
      s:=Copy(s, n+Length('inet addr:'), 999);
    end;
    if Result.Length > 0 then
      Result:=Result + ',';
    Result:=Result+Trim(Copy(s, 1, Pos(' ', s)));
  end;
  {$ENDIF}
  sl.Free();
end;

function GetIpList: string;
var
  i,n,Sd : Integer;
  buf : Array[0..1023] of byte;
  ifc : TIfConf;
  ifr : TIFRec;
  s: string;
begin
  Result := '';
  sd:=fpSocket(AF_INET,SOCK_DGRAM,IPPROTO_IP);
  if (sd<0) then 
    exit;
  Try
    ifc.ifc_len:=Sizeof(Buf);
    ifc.ifcu_buf:=@buf;
    if fpioctl(sd, SIOCGIFCONF, @ifc)<0 then
      Exit;
    n:= ifc.ifc_len;  
    i:=0;
    While (I<N) do
    begin
      ifr := Tifrec(Pointer(PtrInt(@buf) + i)^);
      s := NetAddrToStr(ifr.ifru_addr.sin_addr);
      if result <> '' then
        result := result + ',';
      result := result + s;
        I:=I+sizeof(tifrec);
    end;
  Finally  
    fileClose(sd);
  end;
end;

end.
