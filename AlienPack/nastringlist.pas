unit NAStringList;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics;

type
  TNAStringStyle=class
     Color:TColor;
     FontStyles:TFontStyles;
  end;


  //+перенос по словам
  //+ стиль и цвет шрифта для строки

  { TNAStringList }

  TNAStringList = class(TStringList)
    private
      FNoNeedToPaint: Boolean;
      FWidth: integer;
      FFontSize: Integer;
      FOnChangeProc: procedure(Sender: TObject) of object;
      FGotLastLine: boolean;
   //   procedure WordToLines(Str:String; len: integer; var SL: TStringList);
     // procedure RefreshStrings;
    protected
      function Get(Index: Integer): string; override;
      procedure Put(Index: Integer; const S: string);  override;
    public
      constructor Create;//там OwnsObject:=true
      constructor Create(StrWidth: Integer);
      function Add(const S: string): Integer; override;
      procedure Insert(Index: Integer; const S: string); override;
      procedure AddStrings(TheStrings: TStrings); overload; override;
      procedure SetWidth(AValue: integer);
    public
      class procedure TextToWords(Str: String; var list: TStringList);
   //   procedure WordWrap(Str:String; len: integer; var SL: TStringList);
      class procedure WordWrapPix(Str: String; PixWidth: Integer; FStyles:TFontStyles;
                                  FSize: Integer; var SL: TStringList);
      procedure RefreshStrings;
      procedure SetStringStyle(Index:integer; Color: TColor; FontStyles: TFontStyles);
      procedure SetStringStyle(Index:integer; Style:TNAStringStyle);
      function AddStyled(const S: string; Color: TColor; FontStyles: TFontStyles): Integer;
      function AddStyled(const S: string; Style:TNAStringStyle): Integer;
      procedure InsertStyled(Index: Integer; const S: string; Color: TColor; FontStyles: TFontStyles);
      procedure InsertStyled(Index: Integer; const S: string; Style:TNAStringStyle);
      function GetStringColor(Index: Integer): TColor;
      function GetStringFontStyles(Index: Integer): TFontStyles;
      function GetStringStyle(Index: Integer): TNAStringStyle;
      property StringWidth: integer read FWidth write SetWidth;
      procedure CopyList(var DestList: TStringList);
      property Strings[Index: Integer]: string read Get write Put; default;
      property Width: Integer read FWidth;
      property NoNeedToPaint: Boolean read FNoNeedToPaint;
      procedure DisableOnChange;
      procedure EnableOnChange;
      property FontSize: Integer read FFontSize write FFontSize;
  end;

const PMARKS: set of char = [#44..#47, #58..#59, #130];
      ELINE=#13;
//  end;
implementation

procedure TNAStringList.SetWidth(AValue: integer);
begin
  if FWidth=AValue then Exit;
  FWidth:=AValue;
  DisableOnChange;
  RefreshStrings;
  EnableOnChange;
end;

function TNAStringList.Get(Index: Integer): string;
var l: Integer;
begin
   FGotLastLine:=false;
   Result:=inherited Get(Index);
   l:=Length(Result);
   if l>0 then
     if Result[l]=ELINE then
       begin
        Result:=Copy(Result, 1, l-1);
        FGotLastLine:=true;
       end;
end;

procedure TNAStringList.Put(Index: Integer; const S: string);
begin
   Inherited Put(Index, S);
end;

class procedure TNAStringList.TextToWords(Str: String; var list: TStringList);
var len, i, j: integer;
begin
 len:=Length(Str);
 if len=0 then Exit;
 j:=1;
 repeat
  i:=j;
  //пробелы в начале слова будут включены в него
  //т.о., при объединении добавлять не надо
  while (j<=len) and (Str[j]=' ') do inc(j);
//  j:=i;
  //само слово
  while (j<=len) and (Str[j]<>' ') do inc(j);
  if i<>j then
   list.Add(Copy(Str,i,j-i));
 until j>=len;
end;

{
 перенос по словам с учетом ширины в пикселях
}
class procedure TNAStringList.WordWrapPix(Str: String; PixWidth: Integer; FStyles:TFontStyles;
  FSize: Integer; var SL: TStringList);
var i, l, tw, qw, wleft, Drawn: Integer; tsl:TStringList;
    s, sleft: String; bmp:TBitmap;
begin
  if Str='' then Exit;
  bmp:=TBitmap.Create;
 // bmp.SetSize(PixWidth, PixWidth);
  bmp.Canvas.Font.Style:=FStyles;
  bmp.Canvas.Font.Pitch:=fpFixed;
  if FSize<>0 then
    bmp.Canvas.Font.Size:=FSize;
  tw:=bmp.Canvas.TextWidth(Str);
  qw:=bmp.Canvas.TextWidth('Q');
  if PixWidth<qw then PixWidth:=qw;

  if tw<=PixWidth then
  begin
   //Canv.TextOut(x, y, s);
   SL.Add(Str);
   bmp.Free;
   Exit;
  end;
  tsl:=TStringList.Create;
  TextToWords(Str, tsl);
  Drawn:=0;
  wleft:=PixWidth;
  i:=0;
  s:='';
  repeat
   tw:=bmp.Canvas.TextWidth(tsl.Strings[i]);
   if tw<=wleft then
   begin
    s:=s+tsl.Strings[i];
    wleft:=wleft-tw;
    inc(Drawn);
    inc(i);
   end
   else
   begin
     if Drawn>0 then
      begin //что-то вывели, переносим слово на след.строку
        SL.Add(s);
        s:='';
        wleft:=PixWidth;
        Drawn:=0;
      end
      else //ничего не вывели, слово длинное, разбить
       begin
         sleft:='';
         l:=Length(tsl.Strings[i]);
         repeat
           tw:=bmp.Canvas.TextWidth(tsl.Strings[i]);
           if tw>wleft then
            begin
              sleft:=tsl.Strings[i][l]+sleft;
              tsl.Strings[i]:=Copy(tsl.Strings[i], 1, l-1);
              dec(l);
            end;
         until (tw<=wleft)or(l<2);
         SL.Add(tsl.Strings[i]);
         wleft:=PixWidth;
         if i<tsl.Count-1 then
           tsl.Strings[i+1]:=sleft+tsl.Strings[i+1]
          else
            tsl.Add(sleft);
            //SL.Add(sleft);
         inc(i);
       end;
   end;
  until i>tsl.Count-1;
  if s<>'' then SL.Add(s);
  tsl.Free;
  bmp.Free;
end;


//копирует в StringList, соединяя строки, как были изначально
procedure TNAStringList.CopyList(var DestList: TStringList);
var i, j: Integer; s: String;
begin
  if (Count<1) then Exit;
  DestList.OwnsObjects:=true;
  i:=0;
  repeat
    s:=Strings[i];//склеим строки с одинаковым стилем

    //неа, склеить до той, у которой в конце ELINE, т.е.
    //после обращения к Strings[i] будет FGotLastLine=true
    while not(FGotLastLine) and (i<Count-1) do
     begin
       inc(i);
       s:=s+Strings[i];
     end;

    j:=DestList.Add(s);
    if Assigned(Objects[i]) then
     begin
       DestList.Objects[j]:=TNAStringStyle.Create;
       TNAStringStyle(DestList.Objects[j]).FontStyles:=
          TNAStringStyle(Objects[i]).FontStyles;
       TNAStringStyle(DestList.Objects[j]).Color:=
          TNAStringStyle(Objects[i]).Color;
     end;
    inc(i);
  until i>=Count;
end;

procedure TNAStringList.RefreshStrings;
var i, c: Integer; NewStrings:TStringList;
begin
  if Count<1 then Exit;
  NewStrings:=TStringList.Create;
  CopyList(NewStrings);
  Clear;
  c:=NewStrings.Count;
  for i:=0 to c-1 do
  begin
   if Assigned(NewStrings.Objects[i]) then
     AddStyled(NewStrings.Strings[i], TNAStringStyle(NewStrings.Objects[i]))
    else Add(NewStrings.Strings[i]);
  end;
  NewStrings.Free;
end;

procedure TNAStringList.DisableOnChange;
begin
  FOnChangeProc:=OnChange;
  OnChange:=nil;
end;

procedure TNAStringList.EnableOnChange;
begin
  OnChange:=FOnChangeProc;
end;

constructor TNAStringList.Create;
begin
  FWidth:=10;
  OwnsObjects:=true;
end;

constructor TNAStringList.Create(StrWidth: Integer);
begin
  if StrWidth<1 then FWidth:=10
   else FWidth:=StrWidth;
  OwnsObjects:=true;
  FOnChangeProc:=nil;
  FNoNeedToPaint:=false;
end;

function TNAStringList.Add(const S: string): Integer;
var WrappedSL: TStringList; i:integer;
begin
  if S='' then
   begin
     Inherited Add(S+ELINE);
     Exit;
   end;
  WrappedSL:=TStringList.Create;
  WordWrapPix(S, FWidth, [], FFontSize, WrappedSL);
  i:=0;
  WrappedSL.Strings[WrappedSL.Count-1]:=WrappedSL.Strings[WrappedSL.Count-1]+ELINE;
  while i<WrappedSL.Count do
  begin
   Result:=inherited Add(WrappedSL.Strings[i]);
   inc(i);
  end;
  WrappedSL.Free;
end;

procedure TNAStringList.Insert(Index: Integer; const S: string);
var WrappedSL: TStringList; i:integer;
begin
  WrappedSL:=TStringList.Create;
  WordWrapPix(S, FWidth, [], FFontSize, WrappedSL);
  i:=WrappedSL.Count-1;
  WrappedSL.Strings[i]:=WrappedSL.Strings[i]+ELINE;
  while i>=0 do
  begin
   inherited Insert(Index, WrappedSL.Strings[i]);
   dec(i);
  end;
  WrappedSL.Free;
end;

procedure TNAStringList.AddStrings(TheStrings: TStrings);
var WrappedSL, TmpSL: TStringList; i:integer;
begin
  WrappedSL:=TStringList.Create;
  TmpSL:=TStringList.Create;
  for i:=0 to TheStrings.Count-1 do
  begin
    TmpSL.Clear;
    WordWrapPix(TheStrings.Strings[i], FWidth, [], FFontSize, TmpSL);
    TmpSL.Strings[TmpSL.Count-1]:=TmpSL.Strings[TmpSL.Count-1]+ELINE;
    WrappedSL.AddStrings(TmpSL);
  end;
  inherited AddStrings(WrappedSL);
  TmpSL.Free;
  WrappedSL.Free;
end;

procedure TNAStringList.SetStringStyle(Index: integer; Color: TColor;
  FontStyles: TFontStyles);
begin
  if not(Assigned(Objects[Index])) then
    Objects[Index]:=TNAStringStyle.Create;
  TNAStringStyle(Objects[Index]).Color:=Color;
  TNAStringStyle(Objects[Index]).FontStyles:=FontStyles;
end;

procedure TNAStringList.SetStringStyle(Index: integer; Style: TNAStringStyle);
begin
  SetStringStyle(Index, Style.Color, Style.FontStyles);
end;

function TNAStringList.AddStyled(const S: string; Color: TColor;
  FontStyles: TFontStyles): Integer;
var WrappedSL: TStringList; i:integer;
begin
  WrappedSL:=TStringList.Create;
  WordWrapPix(S, FWidth, FontStyles, FFontSize, WrappedSL);
  WrappedSL.Strings[WrappedSL.Count-1]:=WrappedSL.Strings[WrappedSL.Count-1]+ELINE;
  i:=0;
  FNoNeedToPaint:=true;
  while i<WrappedSL.Count do
  begin
//   FNoNeedToPaint:=true;
   Result:=inherited Add(WrappedSL.Strings[i]);
   Objects[Result]:=TNAStringStyle.Create;
   TNAStringStyle(Objects[Result]).Color:=Color;
   TNAStringStyle(Objects[Result]).FontStyles:=FontStyles;
 {  FNoNeedToPaint:=false;
   Changed;}
   inc(i);
  end;
  WrappedSL.Free;
  FNoNeedToPaint:=false;
  Changed;
end;

function TNAStringList.AddStyled(const S: string; Style: TNAStringStyle
  ): Integer;
var ss:TNAStringStyle;
begin
  if  Assigned(Style) then
    Result:=AddStyled(s, Style.Color, Style.FontStyles)
   else
     begin
       ss:=TNAStringStyle.Create;
       ss.Color:=clDefault; ss.FontStyles:=[];
       Result:=AddStyled(s, ss.Color, ss.FontStyles);
       FreeAndNil(ss);
     end;
end;

procedure TNAStringList.InsertStyled(Index: Integer; const S: string;
  Color: TColor; FontStyles: TFontStyles);
var WrappedSL: TStringList; i:integer;
begin
  WrappedSL:=TStringList.Create;
  WordWrapPix(S, FWidth, FontStyles, FFontSize, WrappedSL);
  i:=WrappedSL.Count-1;
  WrappedSL.Strings[i]:=WrappedSL.Strings[i]+ELINE;
  FNoNeedToPaint:=true;
  while i>=0 do
  begin
   inherited Insert(Index, WrappedSL.Strings[i]);
   Objects[Index]:=TNAStringStyle.Create;
   TNAStringStyle(Objects[Index]).Color:=Color;
   TNAStringStyle(Objects[Index]).FontStyles:=FontStyles;
   dec(i);
  end;
  WrappedSL.Free;
  FNoNeedToPaint:=false;
  Changed;
end;

procedure TNAStringList.InsertStyled(Index: Integer; const S: string;
  Style: TNAStringStyle);
begin
  InsertStyled(Index, s, Style.Color, Style.FontStyles);
end;

function TNAStringList.GetStringColor(Index: Integer): TColor;
begin
  if Assigned(Objects[Index]) then
    Result:=TNAStringStyle(Objects[Index]).Color
   else
     Result:=clDefault;
end;

function TNAStringList.GetStringFontStyles(Index: Integer): TFontStyles;
begin
  if Assigned(Objects[Index]) then
    Result:=TNAStringStyle(Objects[Index]).FontStyles
   else
     Result:=[];
end;

function TNAStringList.GetStringStyle(Index: Integer): TNAStringStyle;
begin
   if Assigned(Objects[Index]) then
    Result:=TNAStringStyle(Objects[Index])
   else
     Result:=nil;
end;


end.

