program Chat;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, uMain, ChatSettings, ChatSettingsType, uLocale
  { you can add units after this }
  , logviewer, alienpack
  {$IfDef WINDOWS}, windows{$EndIf};

{$IfDef WINDOWS}
var fw, Ex: Integer;
{$EndIf}
{$R *.res}
begin
  Application.Title:='NAChat';
  RequireDerivedFormResource := True;
  Application.Initialize;
  {$IfDef WINDOWS}
   fw:=FindWindow(nil, 'NAChat');
   Ex:=GetWindowLong(fw, GWL_EXSTYLE);
   SetWindowLong(fw,GWL_EXSTYLE, Ex or WS_EX_TOOLWINDOW and not WS_EX_APPWINDOW);
  {$EndIf}
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TSetupForm, SetupForm);
  Application.CreateForm(TLogForm, LogForm);
  Application.Run;
end.

