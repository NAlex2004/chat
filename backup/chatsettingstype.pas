unit ChatSettingsType;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Graphics, LMessages;

type
  TChatSettings = record
    Nick: string;
    MsgTime: integer;
    MsgColor: TColor;
    MsgFontColor: TColor;
    MsgPrivFontColor: TColor;
    MainColor: TColor;
    MainFontColor: TColor;
    IncNickColor: TColor;
    OutNickColor: TColor;
    Left, Top, Width, Height: Integer;
    Port: Integer;
    TimeToOffline: Integer;
    UseLog: Boolean;
    Maximized: Boolean;
    UseLastIP: Boolean;
    StartHidden: Boolean;
    LocalIP: String;
  end;

const MSGCOL = $CEFAFA;//clMoneyGreen;
      MSGFCOL = clGreen;
      MSGPRIVCOL = clRed;
      DEFPORT = 44060;
      DEFTIMEOUT = 7;
      FRMCOLOR = clRed;
      FRMMSGCOLOR = clBlack;
      TOCOLOR = clGreen;
      TOMSGCOLOR = clBlack;
      STATUSMSGCOLOR = clGray;
      LM_SHOWMAINFORM = LM_USER+5;

var Settings: TChatSettings;
    PopUpsShown: byte=0;
    NeedAttension: Boolean;

implementation


end.

