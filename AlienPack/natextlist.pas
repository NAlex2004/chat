unit NATextList;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, LCLType,
  LMessages, FPCanvas, LCLIntf, Clipbrd, NAStringList, Menus, natextlistlang,
  ExtCtrls;

type

  TTLDrawStyle=(tlNormal, tlCustomDraw);
  TDrawSelectionEvent = procedure(SelRect: TRect; ClipR: TRect) of object;
  TDrawLineEvent = procedure(Control: TWinControl; Index: Integer;
                             ARect: TRect) of object;

  { TNACustomTextList }

  TNACustomTextList = class(TScrollingWinControl)
  private
    { Private declarations }
    {$IfDef UNIX}
      FTimer: TTimer;
      FMoving: Boolean;
    {$EndIf}
    FCopyPopUpItem: TMenuItem;
    FCutPopUpItem: TMenuItem;
    FDelPopUpItem: TMenuItem;
    FDrawStyle: TTLDrawStyle;
    FOnDrawSelection: TDrawSelectionEvent;
    FOnDrawLine: TDrawLineEvent;
    FLines: TNAStringList;
    FSelection: TRect;
    FOldXY: TPoint;
    FOldSelection: TRect;
    //FClipRect: TRect;
    FSelecting: Boolean;
    FTextSelected: Boolean;
    FSelIndStart: Integer;
    FSelIndEnd: Integer;
    FPaintSelection: Boolean;
    FCreated: Boolean;
    FLinesChanged: Boolean;
    FLoaded: Boolean;
    procedure SetLines(AValue: TNAStringList);
    procedure SetStringWidth;
    procedure StartSelection(X: Integer; Y: Integer);
    procedure MoveSelection(X: Integer; Y: Integer);
    procedure EndSelection;
    procedure DeleteSelection;
    procedure CutSelection;
    procedure DrawSelection(CV:TCanvas; EraseOnly: Boolean);
    procedure SortSelIndexes;
    procedure GetSelIndexesSorted(var StartInd: Integer; var EndInd: Integer);
    procedure CopySelectionToClipboard;
    procedure ClearTextSelection;
    function GetComponentNumber: Integer;
    function IndexInPage(Ind: Integer): Integer;
    {$IfDef UNIX}
     procedure OnTimerMouseMove(Sender: TObject);
    {$EndIf}
  protected
    { Protected declarations }
    procedure Paint; override;
    procedure Loaded; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
      override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure DblClick; override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint
      ): Boolean; override;
    procedure WMSetFocus(var Message: TLMSetFocus); message LM_SETFOCUS;
    procedure WMKillFocus(var Message: TLMKillFocus); message LM_KILLFOCUS;
    procedure SetName(const Value: TComponentName); override;
  protected
    procedure LinesChanged(Sender: TObject);
    function GetLineIndexByPoint(X: Integer; Y: Integer): Integer;
    procedure SetVertScrollBarPage;
    procedure PopUpCopyClick(Sender: TObject);
    procedure PopUpCutClick(Sender: TObject);
    procedure PopUpDelClick(Sender: TObject);
    procedure DoPopUp(Sender: TObject);
    procedure FontChanged(Sender: TObject); override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Lines:TNAStringList read FLines write SetLines;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: integer); override;
    procedure ChangeBounds(ALeft, ATop, AWidth, AHeight: integer; KeepBase: boolean);
      override;
    property DrawStyle: TTLDrawStyle read FDrawStyle write FDrawStyle;
    property OnDrawLine: TDrawLineEvent read FOnDrawLine write FOnDrawLine;
    property OnDrawSelection: TDrawSelectionEvent read FOnDrawSelection write FOnDrawSelection;
    procedure CalculateRange;
  published
    { Published declarations }

  end;

  { TNATextList }

  TNATextList = class(TNACustomTextList)
    published
      property BorderStyle;
      property BorderWidth;
      property BorderSpacing;
      property Align;
      property OnClick;
      property OnMouseDown;
      property OnDblClick;
      property OnMouseUp;
      property Visible;
      property Color;
      property Font;
      property Constraints;
      property DragCursor;
      property DragKind;
      property DragMode;
      property Enabled;
      property OnChangeBounds;
      property OnContextPopup;
      property OnDragDrop;
      property OnDragOver;
      property OnEnter;
      property OnEndDrag;
      property OnExit;
      property OnKeyPress;
      property OnKeyDown;
      property OnKeyUp;
      property OnMouseMove;
      property OnMouseEnter;
      property OnMouseLeave;
      property OnMouseWheel;
      property OnMouseWheelDown;
      property OnMouseWheelUp;
      property OnResize;
      property OnShowHint;
      property OnStartDrag;
      property OnUTF8KeyPress;
      property ParentBidiMode;
      property ParentColor;
      property ParentShowHint;
      property ParentFont;
      property PopupMenu;
      property ShowHint;
      property TabOrder;
      property TabStop;
      property Anchors;
    published
//      property Border;
      property Lines;
      property OnDrawLine;
  end;

const  SEL_COLOR = clBlack;
       TIMERINT = 100;

procedure Register;

implementation
//uses logger;


procedure Register;
begin
  {$I natextlist_icon.lrs}
  RegisterComponents('AlienPack',[TNATextList]);
end;



{ TNACustomTextList }


procedure TNACustomTextList.SetLines(AValue: TNAStringList);
begin
  if FLines=AValue then Exit;
// FLines.Assign(AValue);
 FLines.Text:=AValue.Text;
 Repaint;
end;

procedure TNACustomTextList.SetStringWidth;
var w: Integer;
begin
  if csLoading in ComponentState then Exit;
  if VertScrollBar.Visible then
    w:=VertScrollBar.ClientSizeWithBar
   else
      w:=ClientWidth;
  TNAStringList(Lines).SetWidth(w);
  Repaint;
end;

procedure TNACustomTextList.StartSelection(X: Integer; Y: Integer);
begin

   FSelection:=Rect(X+HorzScrollBar.Position, Y+VertScrollBar.Position,
                    X+HorzScrollBar.Position, Y+VertScrollBar.Position);


   FOldXY:=Point(X, Y);

  FOldSelection:=FSelection;
  FSelIndStart:=GetLineIndexByPoint(FSelection.Left, FSelection.Top);
  FSelIndEnd:=FSelIndStart;

 // Repaint;
  FSelecting:=true;

  {$IfDef UNIX}
    FTimer.Enabled:=true;
  {$Else}
    SetCapture(self.Handle);
  {$EndIf}
end;

procedure TNACustomTextList.MoveSelection(X: Integer; Y: Integer);
var dy, dx: Integer; DoVertScr, DoHorzScr: Boolean; MouseLeaved: Boolean;
    {$IfDef UNIX}
    TimerEnabled: Boolean;
    {$Endif}
begin
  {$IfDef UNIX}
  TimerEnabled:=FTimer.Enabled;
  FTimer.Enabled:=false;
  {$Endif}
  DoVertScr:=true;
  DoHorzScr:=true;
  dy:=Y-FOldXY.y;
  dx:=X-FOldXY.x;
  if ((X<Left) and (dx>0)) or ((X>Left+Width) and (dx<0)) then
   DoHorzScr:=false;
  if ((Y<Top) and (dy>0)) or ((Y>Top+Height) and (dy<0)) then
   DoVertScr:=false;
  if (X<Left) or (X>Left+Width) or (Y<Top) or (Y>Top+Height) then
   begin
    MouseLeaved:=true;
   end
    else MouseLeaved:=false;


  with FSelection do
  begin
    Right:=Right+dx;
    Bottom:=Bottom+dy;
    if MouseLeaved then
     begin
        if (DoVertScr) and (dy<>0) then
         if VertScrollBar.Visible then
          {$IfDef UNIX}
          if ((VertScrollBar.Position+ClientHeight<VertScrollBar.Range)//-VertScrollBar.Page)
               and (dy>0))
          {$Else}
          if ((VertScrollBar.Position<VertScrollBar.Range-VertScrollBar.Page)
               and (dy>0))
          {$Endif}
            or ((VertScrollBar.Position>0) and (dy<0)) then
          begin
           VertScrollBar.Position:=VertScrollBar.Position+dy;
           Bottom:=Bottom+dy;
          end;

        if (DoHorzScr) and (dx<>0) then
         if HorzScrollBar.Visible then
         {$IfDef UNIX}
         if ((HorzScrollBar.Position+ClientWidth<HorzScrollBar.Range-HorzScrollBar.Page)
              and (dx>0))
         {$Else}
          if ((HorzScrollBar.Position<HorzScrollBar.Range-HorzScrollBar.Page)
               and (dx>0))
         {$Endif}
            or ((HorzScrollBar.Position>0) and (dx<0)) then
          begin
           HorzScrollBar.Position:=HorzScrollBar.Position+dx;
           Right:=Right+dx;
          end;
     end;
    FOldXY:=Point(X,Y);
  end;

  FTextSelected:=true;
  FSelIndEnd:=GetLineIndexByPoint(FSelection.Right, FSelection.Bottom);
  FPaintSelection:=true;
  Repaint;
  FPaintSelection:=false;
  FOldSelection:=FSelection;
  {$IfDef UNIX}
  FTimer.Enabled:=TimerEnabled;
  {$Endif}
end;

procedure TNACustomTextList.EndSelection;
begin
  FOldSelection:=FSelection;
  FSelecting:=false;
{  if not(FTextSelected) then
   begin
     FSelIndEnd:=-1;
     FSelIndStart:=-1;
   end;   }
  FPaintSelection:=true;
  RePaint;
  FPaintSelection:=false;
  {$IfDef UNIX}
    FTimer.Enabled:=false;
  {$Else}
    ReleaseCapture;
  {$EndIf}
end;

procedure TNACustomTextList.DeleteSelection;
var i, ind, c: Integer;
begin
  if not(FTextSelected) or (FLines.Count=0) then Exit;
  if FSelIndStart<=FSelIndEnd then ind:=FSelIndStart
   else ind:=FSelIndEnd;
  if ind>=0 then
  begin
    c:=abs(FSelIndStart-FSelIndEnd);
    FLines.DisableOnChange;
    for i:=0 to c do
     FLines.Delete(ind);
    FLines.EnableOnChange;
  end;
  ClearTextSelection;
  FSelecting:=false;
  Repaint;
end;

procedure TNACustomTextList.CutSelection;
begin
  CopySelectionToClipboard;
  DeleteSelection;
end;

procedure TNACustomTextList.DrawSelection(CV:TCanvas; EraseOnly: Boolean);
var PenCol:TColor; PenStyle: TFPPenStyle; PenMode: TFPPenMode; BrushStyle: TFPBrushStyle;
    PenWidth: Integer;
begin
  with CV do
  begin
    PenCol:=Pen.Color;
    PenStyle:=Pen.Style;
    PenMode:=Pen.Mode;
    PenWidth:=Pen.Width;
    BrushStyle:=Brush.Style;
    Brush.Style:=bsClear;
    Pen.Color:=SEL_COLOR;
    Pen.Width:=1;
    Pen.Style:=psDot;
    Pen.Mode:=pmNotXor;
    Rectangle(FOldSelection);
    if not(EraseOnly) then
      Rectangle(FSelection);
    Pen.Color:=PenCol;
    Pen.Style:=PenStyle;
    Pen.Mode:=PenMode;
    Brush.Style:=BrushStyle;
    Pen.Width:=PenWidth;
  end;
  if Assigned(FOnDrawSelection) then FOnDrawSelection(FSelection, CV.ClipRect);
end;

procedure TNACustomTextList.SortSelIndexes;
var i: Integer;
begin
  if FSelIndEnd<FSelIndStart then
   begin
     i:=FSelIndStart;
     FSelIndStart:=FSelIndEnd;
     FSelIndEnd:=i;
   end;
end;

procedure TNACustomTextList.GetSelIndexesSorted(var StartInd: Integer;
  var EndInd: Integer);
begin
  if FSelIndStart<=FSelIndEnd then
   begin
    StartInd:=FSelIndStart;
    EndInd:=FSelIndEnd;
   end
  else
   begin
    StartInd:=FSelIndEnd;
    EndInd:=FSelIndStart;
   end;
end;

procedure TNACustomTextList.CopySelectionToClipboard;
var i, si, ei: Integer; s: String;
begin
  if not(FTextSelected) then Exit;
  GetSelIndexesSorted(si, ei);
  s:='';
  for i:=si to ei do
    s:=s+FLines.Strings[i]+sLineBreak;//ELINE;
  Clipboard.AsText:=s;
end;

procedure TNACustomTextList.ClearTextSelection;
begin
  FTextSelected:=false;
  FSelIndEnd:=-1;
  FSelIndStart:=-1;
end;

function TNACustomTextList.GetComponentNumber: Integer;
var i, c: Integer;
begin
  if Assigned(Parent) then
   with Parent do
   begin
    c:=0;
    for i:=0 to ComponentCount-1 do
      if Components[i] is TNACustomTextList then
        inc(c);
    Result:=c;
   end
  else
   Result:=ComponentIndex;
end;

function TNACustomTextList.IndexInPage(Ind: Integer): Integer;
var Y, th, w: Integer;
begin
  Result:=0;
  th:=Canvas.TextHeight('Q');
  Y:=th*Ind;
  if Y-th<=VertScrollBar.Position then
    Result:=-1;
  if HorzScrollBar.Visible then
    w:=HorzScrollBar.ClientSizeWithBar
    else w:=ClientHeight;
  if Y+th>=VertScrollBar.Position+w then //VertScrollBar.Page then
    Result:=1;
end;

{$IfDef UNIX}
procedure TNACustomTextList.OnTimerMouseMove(Sender: TObject);
var MP:TPoint;
begin
  if not(FSelecting) or FPaintSelection or FMoving then Exit;

  MP:=ScreenToClient(Mouse.CursorPos);
  MP.X:=MP.X-HorzScrollBar.Position;
  MP.Y:=MP.Y-VertScrollBar.Position;
 // MP:=Mouse.CursorPos;
  if (MP.Y=FOldXY.Y) and (MP.X=FOldXY.X) then Exit;
  FMoving:=true;
  MoveSelection(MP.X, mp.Y);
  FMoving:=false;
end;
{$EndIf}


procedure TNACustomTextList.Paint;
var c, y, w, tw, th, StartInd, EndInd, i:integer;
    BackCol, ForgColor: TColor;
begin
  inherited Paint;
  if not(Assigned(FLines)) then  Exit;
  y:=0; w:=Width;
  c:=FLines.Count;
  if c<=0 then Exit;
  //th:=Canvas.TextHeight(FLines.Strings[0]);
  Canvas.Font:=Self.Font;
  th:=Canvas.TextHeight('Q');

  if VertScrollBar.Visible then
   VertScrollBar.Range:=th*(FLines.Count);//Canvas.TextHeight('A')*FLines.Count;
  if HorzScrollBar.Visible then
    HorzScrollBar.Range:=w;

  StartInd:=(Canvas.ClipRect.Top) div th - 1;
  EndInd:=(Canvas.ClipRect.Bottom) div th + 1;

  if StartInd<0 then
   begin
     StartInd:=0;
     y:=0;
   end
   else
     y:=(StartInd-1)*th;

  //if EndInd>c-1 then EndInd:=c-1;
  if EndInd>FLines.Count-1 then EndInd:=FLines.Count-1;
  for i:=StartInd to EndInd do
  begin
    if Assigned(FLines.Objects[i]) then
     begin
        Canvas.Font.Color:=TNAStringStyle(FLines.Objects[i]).Color;
        Canvas.Font.Style:=TNAStringStyle(FLines.Objects[i]).FontStyles;
     end
    else
     begin
       Canvas.Font:=Font;
     end;

    BackCol:=Canvas.Brush.Color;
    ForgColor:=Canvas.Font.Color;
    if FTextSelected then
     begin
        if ((i>=FSelIndStart)and(i<=FSelIndEnd)) or
         ((i<=FSelIndStart)and(i>=FSelIndEnd)) then
          begin
           Canvas.Brush.Color:=clHighlight;
           Canvas.Font.Color:=clHighlightText;
          end;
     end;

    if (FDrawStyle=tlNormal)
      or ((FDrawStyle=tlCustomDraw) and not(Assigned(FOnDrawLine))) then
      Canvas.TextOut(1, y, FLines.Strings[i])
     else
       FOnDrawLine(Self as TWinControl, i, Rect(0, y, Width, y+th-1));

    Canvas.Brush.Color:=BackCol;
    Canvas.Font.Color:=ForgColor;

    tw:=Canvas.TextWidth(FLines.Strings[i]);
    if tw>w then w:=tw;
    y:=y+th;//Canvas.TextHeight(FLines.Strings[i]);
  end;


  if Focused then
    Canvas.DrawFocusRect(Rect(HorzScrollBar.Position, VertScrollBar.Position,
              HorzScrollBar.Position+ClientWidth, VertScrollBar.Position+ClientHeight));
       //(Canvas.ClipRect);

//  FClipRect:=Canvas.ClipRect;

  if FPaintSelection then
  begin
     DrawSelection(Canvas, FSelecting);
  end;
end;

procedure TNACustomTextList.Loaded;
var th: Integer;
begin
  inherited Loaded;
  SetStringWidth;
  th:=Canvas.TextHeight('Q');
  VertScrollBar.Range:=(FLines.Count)*th;
  VertScrollBar.Position:=VertScrollBar.Range-ClientHeight;//+th;
  FLoaded:=true;
end;

procedure TNACustomTextList.KeyDown(var Key: Word; Shift: TShiftState);
var i, NewPosition: Integer; PositionChanged: Boolean;
begin
  inherited KeyDown(Key, Shift);
  i:=0;
  case Key of
   VK_C, VK_INSERT: if ssCtrl in Shift then
                    begin
                       CopySelectionToClipboard;
                       Key:=0;
                       Exit;
                    end;
   VK_X: if ssCtrl in Shift then
                    begin
                       i:=VertScrollBar.Position;
                       CutSelection;
                       if FLines.Count=0 then
                       begin
                         VertScrollBar.Range:=VertScrollBar.Page;
                         VertScrollBar.Position:=0;
                       end
                       else
                        if VertScrollBar.Range>ClientHeight then
                          VertScrollBar.Position:=i
                         else VertScrollBar.Position:=0;
                       Key:=0;
                       Exit;
                    end;
   VK_DELETE: begin
                 i:=VertScrollBar.Position;
                 DeleteSelection;
                 Key:=0;
                 if FLines.Count=0 then
                  begin
                    VertScrollBar.Range:=VertScrollBar.Page;
                    VertScrollBar.Position:=0;
                  end
                 else
                  if VertScrollBar.Range>ClientHeight then
                   VertScrollBar.Position:=i
                   else VertScrollBar.Position:=0;
                 Exit;
              end;
   VK_UP: begin
           i:=-VertScrollBar.Increment;
           Key:=0;
           if ssShift in Shift then
            begin
              FTextSelected:=true;
              if FSelIndStart=-1 then FSelIndStart:=0;
              if not(ssTriple in Shift) then
               begin
                if FSelIndEnd>0 then dec(FSelIndEnd);
                if IndexInPage(FSelIndEnd)>-1 then i:=0;
               end;
            end
            else
             if not(ssTriple in Shift) then
               ClearTextSelection;
          end;
   VK_DOWN:begin
            i:=VertScrollBar.Increment;
            Key:=0;
            if ssShift in Shift then
            begin
              FTextSelected:=true;
              if FSelIndStart=-1 then FSelIndStart:=0;
              if not(ssTriple in Shift) then
              begin
                if FSelIndEnd<FLines.Count-1 then inc(FSelIndEnd);
                if IndexInPage(FSelIndEnd)<1 then i:=0;
              end;
            end
            else
             if not(ssTriple in Shift) then
               ClearTextSelection;
           end;
   VK_HOME:begin
             i:=-VertScrollBar.Position;
             Key:=0;
             if ssShift in Shift then
             begin
               FTextSelected:=true;
               FSelIndEnd:=0;
             end
             else ClearTextSelection;
           end;
   VK_END:begin
             {$IfDef UNIX}
             i:=VertScrollBar.Range-ClientHeight-VertScrollBar.Position;
             {$Else}
             i:=VertScrollBar.Range-VertScrollBar.Page-VertScrollBar.Position;
             {$Endif}
             key:=0;
             if ssShift in Shift then
              begin
                FTextSelected:=true;
                FSelIndEnd:=FLines.Count-1;
              end
              else ClearTextSelection;
          end;
   VK_A: if ssCtrl in Shift then
         begin
           FTextSelected:=true;
           Key:=0;
           FSelIndStart:=0;
           FSelIndEnd:=FLines.Count-1;
         end;
   else
     if not((ssShift in Shift) or (ssCtrl in Shift)) then
     begin
     //  i:=0;
       ClearTextSelection;
     //  Exit;
     end;
  end;
  NewPosition:=VertScrollBar.Position+i;

  {$IfDef UNIX}
  if NewPosition+ClientHeight>VertScrollBar.Range then//-VertScrollBar.Page then
  {$Else}
  if NewPosition>VertScrollBar.Range-VertScrollBar.Page then
  {$EndIf}
  begin
    {$IfDef UNIX}
    NewPosition:=VertScrollBar.Range-ClientHeight;
    {$Else}
    NewPosition:=VertScrollBar.Range-VertScrollBar.Page;
    {$Endif}
    i:=0;
  end;

  if NewPosition<0 then
  begin
    NewPosition:=0;
    i:=0
  end;

  PositionChanged:=not(VertScrollBar.Position=NewPosition);
  VertScrollBar.Position:=NewPosition;

  if FSelecting then
   begin
     FOldSelection:=FSelection;
     FSelection.Bottom:=FSelection.Bottom+i;
     if ssTriple in Shift then
     begin   //   для paint  при скролле
        FSelIndEnd:=GetLineIndexByPoint(FSelection.Right, FSelection.Bottom);
     FPaintSelection:=true;
     Repaint;
     FPaintSelection:=false;
     end;      //      для paint
   end
   else
    if FTextSelected then
     begin
      if not(PositionChanged) then
       Repaint;
     end;
end;

procedure TNACustomTextList.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if not (csDesigning in ComponentState) and CanFocus then
    SetFocus;
  inherited MouseDown(Button, Shift, X, Y);
  if Button=mbLeft then
   begin
     FTextSelected:=false;
     StartSelection(X, Y);
   end;
end;

procedure TNACustomTextList.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);
  {$IfDef WINDOWS}
    if not(FSelecting) then Exit;
    MoveSelection(X, Y);
  {$EndIf}
end;

procedure TNACustomTextList.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if FSelecting then
   EndSelection;
  inherited MouseUp(Button, Shift, X, Y);
end;

procedure TNACustomTextList.DblClick;
begin
  inherited DblClick;
  FTextSelected:=true;
end;


function TNACustomTextList.DoMouseWheel(Shift: TShiftState;
  WheelDelta: Integer; MousePos: TPoint): Boolean;
var dxy: Word;
begin
  Shift:=Shift+[ssTriple];
  if FSelecting then Shift:=Shift+[ssShift];//, ssTriple];
  //ssTriple будем использовать для определения, что это колесиком крутим выделение,
  //а не кнопкой

  if WheelDelta<0 then dxy:=VK_DOWN
   else dxy:=VK_UP;
  KeyDown(dxy, Shift);

  //Result:=inherited DoMouseWheel(Shift, WheelDelta, MousePos);
  Result:=true;
end;



procedure TNACustomTextList.WMSetFocus(var Message: TLMSetFocus);
begin
  Inherited;
  Repaint;
end;

procedure TNACustomTextList.WMKillFocus(var Message: TLMKillFocus);
begin
  inherited;
  Repaint;
end;

procedure TNACustomTextList.SetName(const Value: TComponentName);
begin
  if not(FLoaded) then
   if not(csLoading in ComponentState) and
     (csDesigning in ComponentState) and not(FLinesChanged)
     then
      begin
       Lines.Text:=Value;
       FLinesChanged:=false;
      end;

  inherited SetName(Value);
  {if not(csDesigning in ComponentState) then Exit;

  if not(FLinesChanged) then
   begin
    // if Lines.Count=0 then
      Lines.Text:=Name;
     FLinesChanged:=false;
   end;}
end;



procedure TNACustomTextList.LinesChanged(Sender: TObject);
var w: Integer;
begin

  if VertScrollBar.Visible then
    w:=VertScrollBar.ClientSizeWithBar
   else
      w:=ClientWidth;
  if not(Lines.NoNeedToPaint) then
   if Lines.Width<>w then
    TNAStringList(Lines).SetWidth(w);
//   else
  if not(Lines.NoNeedToPaint) then
     Repaint
     else
      CalculateRange;

 if VertScrollBar.Visible then
   VertScrollBar.Position:=VertScrollBar.Range-ClientHeight;

 FLinesChanged:=true;
end;

function TNACustomTextList.GetLineIndexByPoint(X: Integer; Y: Integer): Integer;
var th: Integer;
begin
  Result:=-1;
  if FLines.Count<1 then Exit;
  Canvas.Font:=Self.Font;
  th:=Canvas.TextHeight('Q');
  Result:=Y div th;
//  if FClipRect.Top>th then inc(result);
  if VertScrollBar.Position>th then inc(result);
//  if Result<0 then Result:=0;
  if Result>=FLines.Count then Result:=FLines.Count-1;
end;



constructor TNACustomTextList.Create(AOwner: TComponent);
begin
  FCreated:=false;
  FLoaded:=false;
  FLinesChanged:=false;
  inherited Create(AOwner);

  FLines:=TNAStringList.Create(Width);
  FLines.OnChange:=@LinesChanged;
  TabStop:=true;
  BorderStyle:=bsSingle;
  Color:=clWindow;
 // Font.Color:=clBtnText;
  DoubleBuffered:=true;
  VertScrollBar.Visible:=true;
  VertScrollBar.Smooth:=false;
  SetVertScrollBarPage;
  HorzScrollBar.Visible:=false;

  FSelecting:=false;
  FTextSelected:=false;
  PopupMenu:=TPopupMenu.Create(self);
  FCopyPopUpItem:=TMenuItem.Create(PopupMenu);
  FCutPopUpItem:=TMenuItem.Create(PopupMenu);
  FDelPopUpItem:=TMenuItem.Create(PopupMenu);
  FCopyPopUpItem.Caption:=SCopy;
  FCutPopUpItem.Caption:=SCut;
  FDelPopUpItem.Caption:=SDelete;
  FCopyPopUpItem.OnClick:=@PopUpCopyClick;
  FCutPopUpItem.OnClick:=@PopUpCutClick;
  FDelPopUpItem.OnClick:=@PopUpDelClick;

  PopupMenu.Items.Add(FCopyPopUpItem);
  PopupMenu.Items.Add(FCutPopUpItem);
  PopupMenu.Items.Add(FDelPopUpItem);
  PopupMenu.OnPopup:=@DoPopUp;
  {$IfDef UNIX}
   FTimer:=TTimer.Create(Self);
   FTimer.Enabled:=false;
   FTimer.Interval:=TIMERINT;
   FTimer.OnTimer:=@OnTimerMouseMove;
   FMoving:=false;
  {$EndIf}
  if AOwner is TWinControl then
   Parent:=AOwner as TWinControl;
  FCreated:=true;
end;



destructor TNACustomTextList.Destroy;
begin
  FLines.Free;
 { FCopyPopUpItem.Free;
  FCutPopUpItem.Free;
  FDelPopUpItem.Free;}
  PopupMenu.Free;
  {$IfDef UNIX}
   FTimer.Enabled:=false;
   FTimer.Free;
  {$EndIf}
  inherited Destroy;
end;

procedure TNACustomTextList.SetVertScrollBarPage;
var h: Integer; bmp: TBitmap;
begin
  bmp:=TBitmap.Create;
  bmp.SetSize(Width, Height);
  h:=bmp.Canvas.TextHeight('Q');
  bmp.Free;
  VertScrollBar.Increment:=h;
  h:=Height-h*2;
  if h>0 then
    VertScrollBar.Page:=h
   else VertScrollBar.Page:=80;
end;

procedure TNACustomTextList.PopUpCopyClick(Sender: TObject);
var Key: Word;
begin
  Key:=VK_C;
  KeyDown(Key, [ssCtrl]);
end;

procedure TNACustomTextList.PopUpCutClick(Sender: TObject);
var Key: Word;
begin
  Key:=VK_X;
  KeyDown(Key, [ssCtrl]);
end;

procedure TNACustomTextList.PopUpDelClick(Sender: TObject);
var Key: Word;
begin
  Key:=VK_DELETE;
  KeyDown(Key, []);
end;

procedure TNACustomTextList.DoPopUp(Sender: TObject);
begin
  FCopyPopUpItem.Enabled:=FTextSelected;
  FCutPopUpItem.Enabled:=FTextSelected;
  FDelPopUpItem.Enabled:=FTextSelected;
end;

procedure TNACustomTextList.FontChanged(Sender: TObject);
begin
  inherited FontChanged(Sender);
  FLines.FontSize:=Font.Size;
  FLines.DisableOnChange;
  FLines.RefreshStrings;
  FLines.EnableOnChange;
  Repaint;
end;

procedure TNACustomTextList.SetBounds(ALeft, ATop, AWidth, AHeight: integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if not(FCreated) then Exit;
  SetVertScrollBarPage;
  Repaint;
end;

procedure TNACustomTextList.ChangeBounds(ALeft, ATop, AWidth, AHeight: integer;
  KeepBase: boolean);
var w: Integer;
begin
  w:=Width;
  inherited ChangeBounds(ALeft, ATop, AWidth, AHeight, KeepBase);
  if w<>AWidth then
    if Assigned(Lines) then
      SetStringWidth;
end;

procedure TNACustomTextList.CalculateRange;
var bmp: TBitmap; h: Integer;
begin
  bmp:=TBitmap.Create;
  bmp.Canvas.Font:=Self.Font;
  h:=bmp.Canvas.TextHeight('Q');
  bmp.Free;
  VertScrollBar.Range:=FLines.Count*h;
end;


end.
