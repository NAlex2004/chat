unit LabEdit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TLabEdit }
{ Просто для ознакомления, компонент, состоящий из метки и поля ввода 2 в 1}

  TLabEdit = class(TCustomControl)
  private

     { Private declarations }
    isLoaded:boolean;
    FLabel:TLabel;
    FEdit:TEdit;
    FTopTmp: integer;
    FLeftTmp: integer;
    FHeightTemp:integer;
    FWidthTemp:integer;
    FOnResize:TNotifyEvent;
    function IntMax(a, b: integer): integer;
  protected
    { Protected declarations }
    procedure Loaded; override; // эту обязательно для задания координат и т.п
    // когда компонент загрузился
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure SetBounds(aLeft, aTop, aWidth, aHeight: integer); override;
    //все Height, Width и т.п. в итоге вызывают это, а это virtual описано,
    //значит, можно сделать override, вызывать действия inherited,
    // и после изменять положение/размеры наших вложенных компонентов
  published
    { Published declarations }
    property Align;     //просто публикуем в инспектор свойства,
    property Anchors;   //ранее в public какого-то предка описанные
    property OnClick;
    property OnResize: TNotifyEvent read FOnResize write FOnResize;
    property xLabel:TLabel read FLabel;
    property xEdit:TEdit read FEdit;
  end;

procedure Register;

implementation

procedure Register;
begin
  {$I labedit_icon.lrs}
  RegisterComponents('AlienPack',[TLabEdit]);
end;

{ TLabEdit }


function TLabEdit.IntMax(a, b: integer): integer;
begin
  if a>=b then result:=a
   else Result:=b;
end;



procedure TLabEdit.Loaded;
begin
  inherited Loaded;
  isLoaded:=true;
  self.SetBounds(FLeftTmp, FTopTmp, FWidthTemp, FHeightTemp);
  // задаем позицию/размер сохраненные
end;

constructor TLabEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FLabel:=TLabel.Create(self);
  FLabel.SetSubComponent(true);
  FLabel.Parent:=self;
  FLabel.Anchors:=[akLeft,akTop, akRight];
  FLabel.Caption:='label';

  FEdit:=TEdit.Create(self);
  FEdit.SetSubComponent(true);
  FEdit.Parent:=self;
  FEdit.Anchors:=[akLeft, akBottom, akRight];
  FEdit.Text:='edit';
  isLoaded:=false;
  self.Constraints.MinHeight:=24;
  self.Constraints.MinWidth:=24;
  self.Height:=46;
  self.Width:=75;
  //дальше обязательно размеры компонентов,
  //а то в designtime хрень рисуется
  FEdit.Top:=24;
  FLabel.Top:=0;
  FLabel.WordWrap:=true;
  FLabel.Width:=75;
  FEdit.Width:=75;
end;

destructor TLabEdit.Destroy;
begin
  FLabel.Free;
  FLabel:=nil;
  FEdit.Free;
  FEdit:=nil;
  inherited Destroy;
end;

procedure TLabEdit.SetBounds(aLeft, aTop, aWidth, aHeight: integer);
begin

  inherited SetBounds(aLeft, aTop, aWidth, aHeight);
  if not isLoaded then
  begin
   //сохраняем параметры. берем максимум для координат.. ну, хз,
    // SetBounds еще чем-то вызывается уже с нулями и в итоге все в lefttop corner
    // а может и сработает уже нормально..
   FLeftTmp:=IntMax(aLeft,FLeftTmp);
   FTopTmp:=IntMax(aTop, FTopTmp);
   FWidthTemp:=aWidth;
   FHeightTemp:=aHeight;
   exit;
  end;

  if isLoaded then
  begin
    FLabel.SetBounds(0, 0, aWidth, IntMax(0, round(aHeight/2)-1));
    FEdit.SetBounds(0, round(aHeight/2)+1, aWidth, IntMax(0, round(aHeight/2)-1));
  end;

end;

end.
