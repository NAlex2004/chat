unit ChatSettings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, Spin, ColorBox, ChatSettingsType, uLocale, LCLType, ipaddr;

type



  { TSetupForm }

  TSetupForm = class(TForm)
    bOK: TButton;
    bCancel: TButton;
    bDefault: TButton;
    cbOutNickColor: TColorBox;
    cbIncNickColor: TColorBox;
    cbMsgColor: TColorBox;
    cbMainColor: TColorBox;
    cbMsgFontColor: TColorBox;
    cbMainFontColor: TColorBox;
    cbMsgPrivColor: TColorBox;
    cbStartHidden: TCheckBox;
    chbLog: TCheckBox;
    cbUseLastIP: TCheckBox;
    cbUseCustomIP: TCheckBox;
    eCustomIP: TEdit;
    eName: TEdit;
    lMainColor: TLabel;
    lIncNickColor: TLabel;
    lMsgPrivColor: TLabel;
    lOutNickColor: TLabel;
    lMsgFontColor: TLabel;
    lMainFontColor: TLabel;
    lName: TLabel;
    lMgsTime: TLabel;
    lMsgColor: TLabel;
    seMsgTime: TSpinEdit;
    procedure bCancelClick(Sender: TObject);
    procedure bDefaultClick(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { private declarations }
    procedure FillDefaultSettings;
  public
    { public declarations }
    function ShowChatSettings(var ChatSettings: TChatSettings): Boolean;
  end;

var
  SetupForm: TSetupForm;

procedure SetDefault(var cs: TChatSettings);

implementation
var ApplySettings: Boolean;

procedure SetDefault(var cs: TChatSettings);
begin
    cs.MsgTime:=3;
    cs.MsgColor:=MSGCOL;
    cs.MsgFontColor:=MSGFCOL;
    cs.MsgPrivFontColor:=MSGPRIVCOL;
    cs.MainColor:=clWindow;
    cs.MainFontColor:=clWindowText;
    cs.IncNickColor:=FRMCOLOR;
    cs.OutNickColor:=TOCOLOR;
    cs.Port:=DEFPORT;
    cs.TimeToOffline:=DEFTIMEOUT;
    cs.UseLog:=false;
    cs.UseLastIP:=true;
    cs.StartHidden:=true;
end;

{$R *.lfm}


{ TSetupForm }

procedure TSetupForm.bOKClick(Sender: TObject);
begin
  if (cbUseCustomIP.Checked and not(IsIPAddress(eCustomIP.Text))) then
  begin
    ShowMessage('Неправильный IP адрес');
    exit;
  end;
  ApplySettings:=true;
  Close;
end;

procedure TSetupForm.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then bCancelClick(Sender);
end;

procedure TSetupForm.FillDefaultSettings;
begin
    cbMsgColor.Selected:=MSGCOL;
    cbMsgFontColor.Selected:=MSGFCOL;
    cbMainColor.Selected:=clWindow;
    cbMainFontColor.Selected:=clWindowText;
    cbMsgPrivColor.Selected:=MSGPRIVCOL;
    cbIncNickColor.Selected:=FRMCOLOR;
    cbOutNickColor.Selected:=TOCOLOR;
    seMsgTime.Value:=3;
    chbLog.Checked:=false;
    cbUseLastIP.Checked:=true;
    cbStartHidden.Checked:=true;
    cbUseCustomIP.Checked:=false;
    eCustomIP.Text:='';
end;

procedure TSetupForm.bCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TSetupForm.bDefaultClick(Sender: TObject);
begin
  if MessageDlg(SResetCaption, SResetSettings, mtConfirmation, mbYesNo, 0) = mrYes then
  begin
    FillDefaultSettings;
  end;
end;

function TSetupForm.ShowChatSettings(var ChatSettings: TChatSettings): Boolean;
begin
  with ChatSettings do
  begin
   cbMsgColor.Selected:=MsgColor;
   cbMsgFontColor.Selected:=MsgFontColor;
   cbMainColor.Selected:=MainColor;
   cbMainFontColor.Selected:=MainFontColor;
   cbMsgPrivColor.Selected:=MsgPrivFontColor;
   cbIncNickColor.Selected:=IncNickColor;
   cbOutNickColor.Selected:=OutNickColor;
   seMsgTime.Value:=MsgTime;
   eName.Text:=Nick;
   chbLog.Checked:=UseLog;
   cbUseLastIP.Checked:=UseLastIP;
   cbStartHidden.Checked:=StartHidden;
   cbUseCustomIP.Checked:=UseCustomIP;
   eCustomIP.Text:=CustomIP;
  end;
  ApplySettings:=false;
  ShowModal;
  if ApplySettings then
  with ChatSettings do
  begin
    MsgColor:=cbMsgColor.Selected;
    MsgFontColor:=cbMsgFontColor.Selected;
    MainColor:=cbMainColor.Selected;
    MainFontColor:=cbMainFontColor.Selected;
    MsgPrivFontColor:=cbMsgPrivColor.Selected;
    IncNickColor:=cbIncNickColor.Selected;
    OutNickColor:=cbOutNickColor.Selected;
    MsgTime:=seMsgTime.Value;
    Nick:=eName.Text;
    UseLog:=chbLog.Checked;
    UseLastIP:=cbUseLastIP.Checked;
    StartHidden:=cbStartHidden.Checked;
    UseCustomIP:=cbUseCustomIP.Checked;
    CustomIP:=eCustomIP.Text;
  end;
  Result:=ApplySettings;
end;

end.

