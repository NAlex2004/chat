unit PrgStatusBar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ComCtrls;

type

  { TPrgStatusBar }
  //Взято из примера, исправлена ошибка
  TPrgStatusBar = class(TStatusBar)
  private
    FProgressBar: TProgressBar;
    FProgressBarPanel: Integer;
    FProgressBarPanelTemp: Integer;
    FIsLoaded: Boolean;
    procedure SetProgressBarPanel(AValue: Integer);
    { Private declarations }
  protected
    procedure Loaded; override;
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property ProgressBar: TProgressBar read FProgressBar;
    property ProgressBarPanel: Integer read FProgressBarPanel write SetProgressBarPanel
             default 1;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('AlienPack',[TPrgStatusBar]);
end;

{ TPrgStatusBar }

procedure TPrgStatusBar.SetProgressBarPanel(AValue: Integer);
var
  i: Integer;
  L: Longint;
begin
  if FProgressBarPanel=AValue then Exit;
  if not FIsLoaded then // Это происходит загрузка свойств из потока
  begin
    FProgressBarPanelTemp := AValue; // Сохраним свойство, чтобы потом присвоить его в Loaded.
    Exit; // Следующая проверка не имеет смысла, пока панели не загружены из потока.
  end;
  if (AValue >= self.Panels.Count) then Exit;
  if AValue = -1 then
  begin
    FProgressBar.Top := 0;
    FProgressBar.Height := 0;
    FProgressBar.Width := 0;
    FProgressBar.Left := 0;
    FProgressBarPanel := -1;
    Exit;
  end;
  FProgressBar.Top := 2;
  FProgressBar.Height := self.Height-2;
  L := 0;
  for i := 1 to AValue do
    L := L + self.Panels[i-1].Width;
  if AValue = 0 then
  begin
    FProgressBar.Left := 0;
  	FProgressBar.Width := self.Panels[AValue].Width;
  end
  else
  begin
    FProgressBar.Left := L + 2;
  	FProgressBar.Width := self.Panels[AValue].Width - 2;
  end;
  FProgressBarPanel:=AValue;

end;

procedure TPrgStatusBar.Loaded;
begin
  inherited Loaded;
  FIsLoaded := True;
  SetProgressBarPanel(FProgressBarPanelTemp);
end;

constructor TPrgStatusBar.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
 // FProgressBarPanel := -1;
  FProgressBar := TProgressBar.Create(Self);
//  Include(TProgressBar(FProgressBar).ComponentStyle, csSubComponent);
  FProgressBar.SetSubComponent(true);
  FProgressBar.Parent := self;
  FProgressBar.Top := 0;
  FProgressBar.Height := 0;
  FProgressBar.Width := 0;
  FProgressBar.Left := 0;
  FProgressBar.Anchors:=[akTop];
  FIsLoaded := False;
end;

destructor TPrgStatusBar.Destroy;
begin
  FProgressBar.Free;
  FProgressBar := nil;
  inherited Destroy;
end;

initialization
 {$I alienpack.lrs}

end.
